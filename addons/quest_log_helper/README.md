# Quest Log Helper Plugin
## Version 1.2
## © Xecestel
## Licensed Under MPL 2.0 (see below)

## Overview
This plugin lets the user easily create quests and objectives to store them in a file inside the game project.  
It gives the user a useful dock that they can use to create their quests and then it stores them in their selected file.  
This plugin is not meant to be used alone, as once the quest has been created it doesn't do anything more, but it's a useful tool to create something you can then manage in your scripts. This plugin was particularly created and designed to perfectly work alongside the [Quest Log Class](https://gitlab.com/Xecestel/quest-log).

## How does it work
This plugin comes with a useful dock that has gives you any button you need to easily configurate and use this plugin.  
- `QUESTS DATA PATH`: enter the path of the `.json` file you want to use to store quests data. Note that if you want to use this plugin alongside the Quest Log Class, you have to make sure the two paths match.
- `OBJECTIVES DATA PATH`: enter the path of the `.json` file you want to use to store objectives data. Note that if you want to use this plugin alongside the Quest Log Class, you have to make sure the two paths match.
- Once a path has been entered, the panel will automatically show every entries on that file. This will be very useful to check which quests and objectives you created and also to edit them: just click on one of them to open it on the Entry panel below. Of course, the panel comes with two tabs, one for the quests and one for the objectives.
- `New Entry panel`: this panel is where the fun begins. Once you have chosen if you want to add a quest or an objective, this panel will show all the fields you will need to fill in order to create your object. Use the New Entry editable field to enter the id (= key in the Dictionary) of the entry you're creating. Note that if you're editing an already existing object, this field will show its id.
- `Add New Entry button`: once you're done with the form, you can click on this button and the plugin will add an entry containing all the information you provided for your entry inside the dictionary in the data file.
- `Reset Entry button`: this button allows you to reset the current entry fields to default in order to start creating a new entry from scratch.

## Notes
This plugin was particularly created and designed to perfectly work alongside the [Quest Log Class](https://gitlab.com/Xecestel/quest-log).  

This plugin was originally made as part of the [RPG Template project](https://gitlab.com/Xecestel/rpg-template).  

The plugin script file and the dock script were made starting from the plugin script file and the dock script file of the [Sound Manager Plugin](https://gitlab.com/Xecestel/sound-manager). Those scripts were designed and written by Simón Olivo (@sarturo) for that project.

## Licenses
Copyright © 2019-2020 Celeste Privitera  

The Quest Log Helper Plugin is subject to the terms of the Mozilla Public License, v. 2.0.  
If a copy of the MPL was not distributed with this file, You can obtain one at [https://mozilla.org/MPL/2.0/](https://mozilla.org/MPL/2.0/).

## Changelog
### Version 0.1
- Started working on the plugin.

### Version 0.2
- Created dock and started working on it
- README updated

### Version 0.3
- Updated entries panel: now it gets populated
- General fixes and improvements

### Version 0.4
- Added quests and objectives entry dictionaries
- Added edit buttons for already existing quests and objectives
- Added files saving feature
- The entry dictionary is automatically updated as you fill the form (WIP)
- Added Reset Entry button
- Added new note on README

### Version 0.5
- Added Delete entry button
- To be fixed: menu buttons adding items infinite times
- Bug fixes and general improvements
- Improved code readability

### Version 0.6
- Improved entry creation: now you don't need to press Enter when you fill out a field to save the text
- Completed entry adding feature
- Bug fixes and general improvements
- Improved code readability

### Version 0.6.1
- Fixes menu buttons adding items infinite times bug

### Version 0.7
- Added Objectives list field
- Script completed; left to do: change icons

### Version 1.0
- Icons changed

### Version 1.1
- Fixed compatibility issues with Godot Engine 3.2

### Version 1.2
- Redesigned the saving and loading from file methods to work with just two files (one for quests, one for objectives) instead of directories, to make the plugin compatible with versions >0.5 of the [Quest Log Class](https://gitlab.com/Xecestel/quest-log)
- Changed the plugin name to Quest Log Helper.
- Updated README
