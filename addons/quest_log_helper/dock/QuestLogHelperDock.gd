#############################################################
#	QUEST LOG HELPER PLUGIN FOR GODOT ENGINE 3			#
#	© Xecestel 2020; licensed under MPL 2.0					#
#	Version 1.2												#
##########################################################################
# This Source Code Form is subject to the terms of the Mozilla Public	#
# License, v. 2.0. If a copy of the MPL was not distributed with this	#
# file, You can obtain one at https://mozilla.org/MPL/2.0/. 			#
##########################################################################
tool
extends ScrollContainer

# Constants
const SETTINGS_FILE = "res://addons/quest_log_helper/QuestLogHelper.json";

const QUEST_TYPES = [ "Primary", "Secondary" ];
const QUEST_STATES = [ "Active", "Inactive", "Completed", "Failed" ];
const OBJECTIVE_STATES = [ "Active", "Inactive", "Completed", "Failed" ];

const DEFAULT_QUEST									: Dictionary = {
	"name" : "",
	"description" : "",
	"type" : "",
	"state" : "",
	"contractor_name" : "",
	"contractor_location" : "",
	"contractor_icon_path" : "",
	"level" : 0,
	"objectives" : []
	};
	
const DEFAULT_OBJECTIVE								: Dictionary = {
	"name" : "",
	"description" : "",
	"quest_name" : "",
	"location" : "",
	"state" : "",
	"level" : 0,
	"optional" : false
	};
###########

# Variables
# Path configuration section
onready var change_quest_path_button		= self.get_node("VBoxContainer/QuestPathPanel/PathSelectionBtn");
onready var quest_dir_field					= self.get_node("VBoxContainer/QuestPathPanel/PathField");
onready var change_objective_path_button	= self.get_node("VBoxContainer/ObjectivePathPanel/PathSelectionBtn");
onready var objective_dir_field				= self.get_node("VBoxContainer/ObjectivePathPanel/PathField");

# Entry type buttons
onready var quests_button					= self.get_node("VBoxContainer/EntryContainer/Quests");
onready var objectives_button				= self.get_node("VBoxContainer/EntryContainer/Objectives");

# Tab section
onready var tab								= self.get_node("VBoxContainer/TabContainer");
onready var quests_files_tab				= self.get_node("VBoxContainer/TabContainer/QuestsScrollContainer/QuestsFiles");
onready var objectives_files_tab			= self.get_node("VBoxContainer/TabContainer/ObjectivesScrollContainer/ObjectivesFiles");

# Entries section
onready var entry_name_field				= self.get_node("VBoxContainer/EntryFileNameField");
onready var add_entry_button				= self.get_node("VBoxContainer/AddEntryButton");
onready var reset_entry_button				= self.get_node("VBoxContainer/ResetEntryButton");

# Quest entries
onready var quest_panel						= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/QuestPanel");
onready var quest_name						= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/QuestPanel/NameField");
onready var quest_description				= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/QuestPanel/DescriptionField");
onready var quest_type						= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/QuestPanel/TypeMenu");
onready var quest_state						= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/QuestPanel/StateMenu");
onready var quest_contractor_name			= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/QuestPanel/ContractorNameField");
onready var quest_contractor_location		= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/QuestPanel/ContractorLocationField");
onready var quest_contractor_icon			= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/QuestPanel/ContractorIconPathField");

# Objective entries
onready var objective_panel					= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/ObjectivePanel");
onready var objective_name					= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/ObjectivePanel/NameField");
onready var objective_description			= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/ObjectivePanel/DescriptionField");
onready var objective_quest_name			= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/ObjectivePanel/QuestNameField");
onready var objective_location				= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/ObjectivePanel/LocationField");
onready var objective_state					= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/ObjectivePanel/StateMenu");
onready var objective_level					= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/ObjectivePanel/LevelBox");
onready var objective_optional				= self.get_node("VBoxContainer/EntryDictionariesContainer/EntryPanel/ObjectivePanel/OptionalBox");

# Internal variables
var TITLE									: String	= "QuestLogHelper" ;
var QUESTS_DATA_PATH						: String	= "";
var OBJECTIVES_DATA_PATH					: String	= "";
var current_entry_type						: String	= "Quest";
var data_settings							: Dictionary;

var QUEST									: Dictionary = DEFAULT_QUEST;
var OBJECTIVE								: Dictionary = DEFAULT_OBJECTIVE;

var QUESTS_DATA								: Dictionary;
var OBJECTIVES_DATA							: Dictionary;
	
#########################

# Signals
signal change_file_requested(entry_type);
signal check_file_names_requested;
#########

func _ready() -> void:
	self.rect_size.x = get_node("VBoxContainer/TabContainer").rect_size.x + 25;
	self.populate_menu_buttons();
	self.read_settings();
	self.get_data();
	self.connect_signals();


func connect_signals() -> void:
	# Path fields and buttons
	quest_dir_field				.connect("text_entered", self, "_on_quest_path_field_text_entered");
	objective_dir_field			.connect("text_entered", self, "_on_objectives_path_field_text_entered");
	change_quest_path_button	.connect("pressed", self, "_on_Quests_PathSelectionBtn_pressed");
	change_objective_path_button.connect("pressed", self, "_on_Objectives_PathSelectionBtn_pressed");

	# Entries type buttons
	quests_button				.connect("pressed", self, "_on_entry_button_pressed", ["Quest"]);
	objectives_button			.connect("pressed", self, "_on_entry_button_pressed", ["Objective"]);
	
	# Add and reset entry button
	add_entry_button			.connect("pressed", self, "_on_add_entry_button_pressed");
	reset_entry_button			.connect("pressed", self, "_on_reset_entry_button_pressed");


func populate_menu_buttons() -> void:
	# Populate items of quest_type button
	if (quest_type.get_item_count() == 0):
		for type in QUEST_TYPES:
			self.quest_type.add_item(type);

	# Populate items of quest_state button
	if (quest_state.get_item_count() == 0):
		for state in QUEST_STATES:
			self.quest_state.add_item(state);
	
	# Populate items of objective_state button
	if (objective_state.get_item_count() == 0):
		for state in OBJECTIVE_STATES:
			self.objective_state.add_item(state);


#Creates an entry based on the current state of the selected form
func create_entry() -> Dictionary:
	var ENTRY : Dictionary = {};
	if (current_entry_type == "Quest"):
		ENTRY["name"]						= quest_name.get_text();
		ENTRY["description"]				= quest_description.get_text();
		ENTRY["type"]						= quest_type.get_text();
		ENTRY["state"]						= quest_state.get_text();
		ENTRY["contractor_name"]			= quest_contractor_name.get_text();
		ENTRY["contractor_location"]		= quest_contractor_location.get_text();
		ENTRY["contractor_icon_path"]		= quest_contractor_icon.get_text();
	elif (current_entry_type == "Objective"):
		ENTRY["quest_name"]					= objective_quest_name.get_text();
		ENTRY["name"]						= objective_name.get_text();
		ENTRY["description"]				= objective_description.get_text();
		ENTRY["location"]					= objective_location.get_text();
		ENTRY["state"]						= objective_state.get_text();
		ENTRY["level"]						= objective_level.get_value();
		ENTRY["optional"]					= objective_optional.is_pressed();
	return ENTRY;


#############################
#		FILES HANDLING		#
#############################
func get_data() -> void:
	var file = File.new();
	QUESTS_DATA = read_from_file(QUESTS_DATA_PATH);
	OBJECTIVES_DATA = read_from_file(OBJECTIVES_DATA_PATH);
	self.populate_files_tab("Quest");
	self.populate_files_tab("Objective");


func parse_json_string(json_file: String) -> Dictionary:
	var result: Dictionary = {};
	var json : JSONParseResult = JSON.parse(json_file);
	if (typeof(json.result) == TYPE_DICTIONARY):
		result = json.result;
	else:
		print_debug("Error parsing the JSON file: " + json.get_error_string());
	return result;


# Reads plugin settings
func read_settings() -> void:
	data_settings = self.read_from_file(SETTINGS_FILE);

	self.set_quests_data_path(data_settings["QUESTS_DATA_PATH"]);
	self.set_objectives_data_path(data_settings["OBJECTIVES_DATA_PATH"]);
	
	self.update_gui();


# Updates saved settings
func update_settings() -> void:
	data_settings["QUESTS_DATA_PATH"] = self.get_quests_data_path();
	data_settings["OBJECTIVES_DATA_PATH"] = self.get_objectives_data_path();
	self.store_on_file(SETTINGS_FILE, data_settings);
	self.update_gui();


# Stores data on a file
func store_on_file(PATH : String, DATA : Dictionary) -> void:
	var file = File.new();
	file.open(PATH, File.WRITE)
	file.store_line(to_json(DATA));
	file.close();


# Reads data from a file
func read_from_file(PATH : String) -> Dictionary:
	var file = File.new();
	var DATA : Dictionary;
	if (file.file_exists(PATH)):
		file.open(PATH, File.READ);
		DATA = parse_json_string(file.get_as_text());
		file.close();
	return DATA;


# Updates dock GUI
func update_gui() -> void:
	self.quest_dir_field.set_text(self.get_quests_data_path());
	self.objective_dir_field.set_text(self.get_objectives_data_path());
	self.populate_entries_panel();
	
	if (QUESTS_DATA != null && OBJECTIVES_DATA != null):
		self.populate_files_tab(current_entry_type);


# Files Tab handler
func populate_files_tab(entry_type: String)->void:
	var files_tab : VBoxContainer;
	var entries_data : Dictionary;
	match entry_type:
		"Quest":
			files_tab = quests_files_tab;
			entries_data = QUESTS_DATA;
		"Objective":
			files_tab = objectives_files_tab;
			entries_data = OBJECTIVES_DATA;
	# Cleans the file_tab selected
	if (files_tab is Node):
		while (files_tab.get_child_count() > 0):
			var child_node = files_tab.get_child(0);
			files_tab.remove_child(child_node);
			child_node.queue_free();
		# Populate the file_tab selected
		for entry in entries_data.keys():
			var file_name_container : HBoxContainer = HBoxContainer.new();
			var edit_button : ToolButton = ToolButton.new();
			var edit_icon : Texture = load("res://addons/quest_log_helper/dock/assets/edit_entry.svg") as Texture;
			var delete_button : ToolButton = ToolButton.new();
			var delete_icon : Texture = load("res://addons/quest_log_helper/dock/assets/remove_entry.svg") as Texture;
			var file_label : Label = Label.new();
			# Set the nodes
			file_name_container.set_alignment(BoxContainer.ALIGN_CENTER);
			file_label.set_custom_minimum_size(Vector2(280,0));
			file_label.set_text(entry);
			edit_button.set_button_icon(edit_icon);
			edit_button.set_tooltip("Edit entry");
			delete_button.set_button_icon(delete_icon);
			delete_button.set_tooltip("Delete entry");
			# Add the nodes into the dock scene
			file_name_container.add_child(file_label);
			file_name_container.add_child(edit_button);
			file_name_container.add_child(delete_button);
			files_tab.add_child(file_name_container);
			# Make the signal connections
			edit_button.connect("pressed", self, "_on_edit_button_pressed", [file_label.text])
			delete_button.connect("pressed", self, "_on_delete_button_pressed", [file_label.text]);


#Populates the Entries panel
func populate_entries_panel():
	# Populates quest entries panel
	self.quest_name.set_text(QUEST["name"]);
	self.quest_description.set_text(QUEST["description"]);
	self.quest_type.select(QUEST_STATES.find(QUEST["type"]));
	self.quest_state.select(QUEST_TYPES.find(QUEST["state"]));
	self.quest_contractor_name.set_text(QUEST["contractor_name"]);
	self.quest_contractor_location.set_text(QUEST["contractor_location"]);
	self.quest_contractor_icon.set_text(QUEST["contractor_icon_path"]);
	
	# Populates objective entries panel
	self.objective_name.set_text(OBJECTIVE["name"]);
	self.objective_description.set_text(OBJECTIVE["description"]);
	self.objective_quest_name.set_text(OBJECTIVE["quest_name"]);
	self.objective_location.set_text(OBJECTIVE["location"]);
	self.objective_state.select(OBJECTIVE_STATES.find(OBJECTIVE["state"]));
	self.objective_optional.set_pressed(OBJECTIVE["optional"]);
	
#################################
#		SETTERS AND GETTERS		#
#################################
#Sets a new title to the dock
func set_title(title : String) -> void:
	self.TITLE = title;

#Returns the title of the dock
func get_title() -> String:
	return self.TITLE;

#Sets a new path for the quests data directory
func set_quests_data_path(path : String) -> void:
	self.QUESTS_DATA_PATH = path;

#Return the path of the quests data directory
func get_quests_data_path() -> String:
	return self.QUESTS_DATA_PATH;

#Sets a new path for the objectives data directory
func set_objectives_data_path(path : String) -> void:
	self.OBJECTIVES_DATA_PATH = path;

#Returns the objectives data directory
func get_objectives_data_path() -> String:
	return self.OBJECTIVES_DATA_PATH;

#########################################
#		INTERNAL SIGNALS HANDLING		#
#########################################

# Paths management signals handling
func _on_quest_path_field_text_entered(new_path : String) -> void:
	self.set_quests_data_path(new_path);
	self.emit_signal("check_file_names_requested");
	self.get_data();
	self.update_settings();

func _on_objectives_path_field_text_entered(new_path : String) -> void:
	self.set_objectives_data_path(new_path);
	self.emit_signal("check_file_names_requested");
	self.get_data();
	self.update_settings();

func _on_Quests_PathSelectionBtn_pressed() -> void:
	emit_signal("change_file_requested", "Quest");
	
func _on_Objectives_PathSelectionBtn_pressed() -> void:
	emit_signal("change_file_requested", "Objective");


# Entry panel signals handling
func _on_entry_button_pressed(entry_type : String) -> void:
	if (entry_type == "Quest"):
		tab.current_tab = 0;
		quest_panel.set_visible(true);
		objective_panel.set_visible(false);
		self.current_entry_type = "Quest";
	elif (entry_type == "Objective"):
		tab.current_tab = 1;
		quest_panel.set_visible(false);
		objective_panel.set_visible(true);
		self.current_entry_type = "Objective";


func _on_edit_button_pressed(entry_id : String) -> void:
	entry_name_field.set_text(entry_id);
	if (current_entry_type == "Quest"):
		QUEST = QUESTS_DATA[entry_id];
	elif (current_entry_type == "Objective"):
		OBJECTIVE = OBJECTIVES_DATA[entry_id];
	self.update_gui();


func _on_delete_button_pressed(entry_id : String) -> void:
	match current_entry_type:
		"Quest":
			QUESTS_DATA.erase(entry_id);
			self.store_on_file(QUESTS_DATA_PATH, QUESTS_DATA);
		"Objective":
			OBJECTIVES_DATA.erase(entry_id);
			self.store_on_file(OBJECTIVES_DATA_PATH, OBJECTIVES_DATA);
#	self.emit_signal("check_file_names_requested");
	self.update_gui();


func _on_add_entry_button_pressed() -> void:
	var new_entry = self.create_entry();
	var entry_id : String;
	
	entry_id = entry_name_field.get_text();
	if (entry_id == "" || entry_id == null):
		entry_id = "New" + current_entry_type;
		var i = 0;
		for entry_name in QUESTS_DATA.keys():
			if (entry_id + str(i) == entry_name):
				i += 1;
		entry_id += str(i);
		
	match current_entry_type:
		"Quest":
			QUEST = new_entry;
			QUESTS_DATA[entry_id] = QUEST;
			self.store_on_file(QUESTS_DATA_PATH, QUESTS_DATA);
		"Objective":
			OBJECTIVE = new_entry;
			OBJECTIVES_DATA[entry_id] = OBJECTIVE;
			self.store_on_file(OBJECTIVES_DATA_PATH, OBJECTIVES_DATA);
	self.emit_signal("check_file_names_requested");
	self.update_gui();


func _on_reset_entry_button_pressed() -> void:
	entry_name_field.set_text("");
	if (current_entry_type == "Quest"):
		QUEST = DEFAULT_QUEST;
	if (current_entry_type == "Objective"):
		OBJECTIVE = DEFAULT_OBJECTIVE;
	self.update_gui();

#####################################
#		PLUGIN SIGNALS HANDLING		#
#####################################

func _on_quests_file_changed(path : String) -> void:
	self.set_quests_data_path(path);
	self.get_data();
	populate_files_tab("Quest");
	update_settings();

func _on_objectives_file_changed(path : String) -> void:
	self.set_objectives_data_path(path);
	self.get_data();
	populate_files_tab("Objective");
	update_settings();

func _on_file_names_updated() -> void:
	self.get_data();
	populate_files_tab("Quest");
	populate_files_tab("Objective");
