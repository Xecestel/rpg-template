tool
extends EditorPlugin

# Variables

var dock : Node;
var file_dialog : EditorFileDialog = EditorFileDialog.new(); #Popup window for folder selection on the FileSystem
var entry_type_change_requested : String = "";

###########


# Signals

signal quests_file_changed(new_path);
signal objectives_file_changed(new_path);
signal file_names_updated();

#########


# Set the Sound Manager Module scene as autoload, instance a new dock scene and configure the EditorFileDialog instance
func _enter_tree():
	dock = preload("res://addons/quest_log_helper/dock/QuestLogHelperDock.tscn").instance();
	dock.set_name("QuestLogHelper");
	add_control_to_dock(DOCK_SLOT_LEFT_UL, dock);
	get_editor_interface().get_base_control().add_child(file_dialog);
	file_dialog.mode = FileDialog.MODE_OPEN_FILE;
	self.connect_signals();


# Quit the Sound Manager Module scene as autoload, remove the dock scene and free the 'file_dialog' and 'dock' variables from memory
func _exit_tree():
	remove_control_from_docks(dock);
	dock.queue_free();


# Connects internal and external signals to relative methods
func connect_signals() -> void:
	self.connect("quests_file_changed", dock, "_on_quests_file_changed");
	self.connect("objectives_file_changed", dock, "_on_objectives_file_changed");
	self.connect("file_names_updated", dock, "_on_file_names_updated");
	dock.connect("change_file_requested", self, "_on_change_file_requested");
	dock.connect("check_file_names_requested", self, "_on_check_file_names_requested");
	file_dialog.get_ok().connect("pressed", self, "_on_res_file_pressed");
	file_dialog.connect("file_selected", self, "_on_res_file_selected");
	get_editor_interface().get_resource_filesystem().connect("filesystem_changed", self, "_on_filesystem_changed");


#####################################
#		FILE SYSTEM HANDLING		#
#####################################

func check_file_names_from_paths():
	emit_signal("file_names_updated");


# Signal handlers

func _on_change_file_requested(entry_type : String) -> void:
	file_dialog.popup_centered_ratio();
	entry_type_change_requested = entry_type;

# Triggered when user selects a file on the FileDialog clicking on the Open button
func _on_res_file_pressed() -> void:
	var path : String = file_dialog.get_current_path();
	self._on_res_file_selected(path);

# Triggered when user selects a file on the FileDialog double pressing it
func _on_res_file_selected(path : String) -> void:
	if (entry_type_change_requested == "Quest"):
		emit_signal("quests_file_changed", path);
	elif (entry_type_change_requested == "Objective"):
		emit_signal("objectives_file_changed", path);


func _on_filesystem_changed():
	self.check_file_names_from_paths();


func _on_check_file_names_requested():
	self.check_file_names_from_paths();
