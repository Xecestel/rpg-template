extends KinematicBody2D


# Variables

export (Vector2) var sprite_size = Vector2(32, 32);

onready var party = Party.new();
onready var move_by_units : bool = ProjectSettings.get_setting("Template Settings/General Settings/movement_by_unit");

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
