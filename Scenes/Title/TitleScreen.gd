extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	print_debug(OS.get_user_data_dir())
	$Soundtrack.play();
	$CommandWindow/Commands/NewGameBtn.grab_focus();
	var string = "Ciao, /C[Template], quante /I[Template] hai con te? Forse /V[var1]";


# Fires when the exit button gets pressed
func _on_ExitBtn_pressed():
	self.get_tree().quit();
