extends Control

#variables#
export(String, FILE, "*.tscn") var status_scene;
export(String, FILE, "*.tscn") var equip_scene;
export(String, FILE, "*.tscn") var title_screen;

var party_window_focus = false;
var active_party_button;
var active_command_button;
onready var description_text = $Pause/DescriptionWindow/Label;

# Called when the node enters the scene tree for the first time.
func _ready():
	if (GlobalVariables.formation.size() >= 2):
		$Pause/PartyWindow/PartyMember2.visible = true;
	if (GlobalVariables.formation.size() >= 3):
		$Pause/PartyWindow/PartyMember3.visible = true;
	if (!GlobalVariables.formation.empty()):
		$Pause/PartyWindow/PartyMember1/Portrait.texture = GlobalVariables.formation[0].face;
	$Pause/CommandWindow/VBoxContainer/StatusBtn.grab_focus();

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if ($Pause/CommandWindow/VBoxContainer/StatusBtn.is_hovered()):
		$Pause/CommandWindow/VBoxContainer/StatusBtn.grab_focus();
		description_text.text = "See your party members status";
	
	if ($Pause/CommandWindow/VBoxContainer/ItemsBtn.is_hovered()):
		$Pause/CommandWindow/VBoxContainer/ItemsBtn.grab_focus();
		description_text.text = "Open the inventory";
		
	if ($Pause/CommandWindow/VBoxContainer/EquipBtn.is_hovered()):
		$Pause/CommandWindow/VBoxContainer/EquipBtn.grab_focus();
		description_text.text = "Changes your party members equipment";
		
	if ($Pause/CommandWindow/VBoxContainer/FormationBtn.is_hovered()):
		$Pause/CommandWindow/VBoxContainer/FormationBtn.grab_focus();
		description_text.text = "Manage your party formation";
		
	if ($Pause/CommandWindow/VBoxContainer/OptionsBtn.is_hovered()):
		$Pause/CommandWindow/VBoxContainer/OptionsBtn.grab_focus();
		description_text.text = "Opens the Options Menu";
		
	if ($Pause/CommandWindow/VBoxContainer/SaveBtn.is_hovered()):
		$Pause/CommandWindow/VBoxContainer/SaveBtn.grab_focus();
		description_text.text = "Save the game";
	
	#CUSTOM BUTTONS#
	if ($Pause/CommandWindow/VBoxContainer/CustomBtn1.is_hovered()):
		$Pause/CommandWindow/VBoxContainer/CustomBtn1.grab_focus();
		description_text.text = "Custom Button 1";
		
	if ($Pause/CommandWindow/VBoxContainer/CustomBtn2.is_hovered()):
		$Pause/CommandWindow/VBoxContainer/CustomBtn2.grab_focus();
		description_text.text = "Custom Button 2";
		
	if ($Pause/CommandWindow/VBoxContainer/QuitBtn.is_hovered()):
		$Pause/CommandWindow/VBoxContainer/QuitBtn.grab_focus();
		description_text.text = "Quits to Title Screen";

func _input(event):
	if(party_window_focus):
		if (event.is_action_pressed("ui_cancel")):
			party_window_focus = false;
			self.lose_focus($Pause/PartyWindow);
			self.reactivate_focus($Pause/CommandWindow/VBoxContainer);
			active_command_button.grab_focus();
			

func _on_StatusBtn_pressed():
	active_command_button		= $Pause/CommandWindow/VBoxContainer/StatusBtn;
	self.to_party_window();

func _on_EquipBtn_pressed():
	active_command_button		= $Pause/CommandWindow/VBoxContainer/EquipBtn;
	self.to_party_window();

#TO DO: COMPLETE
func _on_PartyMember_pressed():
	if (active_command_button == $Pause/CommandWindow/VBoxContainer/StatusBtn):
		#opens status screen
		pass
	if (active_command_button == $Pause/CommandWindow/VBoxContainer/EquipBtn):
		#opens equipment screen
		pass
	pass
	
func reactivate_focus(node_list : Node) -> void:
	for child in node_list.get_children():
		child.mouse_filter	= MOUSE_FILTER_STOP;
		child.focus_mode	= FOCUS_ALL;
		
func lose_focus(node_list : Node) -> void:
	for child in node_list.get_children():
		child.mouse_filter	= MOUSE_FILTER_IGNORE;
		child.focus_mode	= FOCUS_NONE;
		
func to_party_window() -> void:
	party_window_focus			= true;
	active_party_button			= $Pause/PartyWindow/PartyMember1;
	self.reactivate_focus($Pause/PartyWindow);
	self.lose_focus($Pause/CommandWindow/VBoxContainer);
	active_party_button.grab_focus();
