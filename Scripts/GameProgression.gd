extends Node
# This script is an AutoLoad that handles the progression of the game.
# What it does is basically storing variables and switches that the user can
# use to make the game progress.
# Use the ProjectSettings to edit the Variables and the Switches dictionaries.

#constants#
const VARIABLES_SETTINGS = "Template Settings/Variables and Switches/Variables";
const SWITCHES_SETTINGS = "Template Settings/Variables and Switches/Switches"
###########

#variables#
# You can also edit this dictionaries from the ProgressData file in the path above

# The Dialog manager saves here every completed dialogue
var Dialogues = {
	"DangerousToGoAlone" : false, # This is just an example
}

# You can use this dictionary to store any variable you like
onready var Variables = ProjectSettings.get_setting(VARIABLES_SETTINGS);

# You can use this dictionary to separate boolean switches from other types of variables
onready var Switches = ProjectSettings.get_setting(SWITCHES_SETTINGS);

#Internal variables
var data_loaded = false;

####################

#########################
#	VARIABLES HANDLING	#
#########################

func set_variable(variable_id : String, variable_value) -> void:
	Variables[variable_id] = variable_value;
	
func has_variable(variable_id : String) -> bool:
	return Variables.has(variable_id);

func get_variable(variable_id : String, default = null):
	return Variables.get(variable_id, default);
	
func get_variable_dictionary() -> Dictionary:
	return Variables;

func set_variable_dictionary(data_dict : Dictionary) -> void:
	Variables = data_dict;

	
#########################
#	SWITCHES HANDLING	#
#########################

func set_switch(switch_id : String, switch_value : bool) -> void:
	Switches[switch_id] = switch_value;

func has_switch(switch_id : String) -> bool:
	return Switches.has(switch_id);
	
func get_switch(switch_id : String, default = null):
	return Switches.get(switch_id, default);
	
func get_switch_dictionary() -> Dictionary:
	return Switches;

func set_switch_dictionary(data_dict : Dictionary) -> void:
	Switches = data_dict;


#########################
#	DIALOGUES HANDLING	#
#########################

func set_dialogue(dialogue_id : String, dialogue_value : bool) -> void:
	Dialogues[dialogue_id] = dialogue_value;

func has_dialogue(dialogue_id : String) -> bool:
	return Dialogues.has(dialogue_id);
	
func get_dialogue(dialogue_id : String, default = null):
	return Dialogues.get(dialogue_id, default);

func get_dialogue_dictionary() -> Dictionary:
	return Dialogues;

func set_dialogue_dictionary(data_dict : Dictionary) -> void:
	Dialogues = data_dict;
	
	
#########################
#	SAVING AND LOADING	#
#########################	
#Returns a save dictionary for saving purposes	
func save():
	var save_dict = {
		"Dialogues"					:	get_dialogue_dictionary(),
		"Variables"					:	get_variable_dictionary(),
		"Switches"					:	get_switch_dictionary()
		}
	return save_dict;

#Loads data from save file
func load_data(save_file : File) -> bool:
	if (save_file == null):
		return false;
	
	var current_line = parse_json(save_file.get_line());
	
	for i in current_line.keys():
		self.set(i, current_line[i]);
		
	data_loaded = true;
	return true;
