extends Entry
class_name Quest

# Variables

var type : String;
var state : String;
var contractor_name : String;
var contractor_location : String;
var contractor_icon_path : String;
var level : int;
var objectives : Array;

var contractor_icon = null;
var completed_objectives = 0;
var started = false;

###########

# Quest constructor
func _init(QUEST : Dictionary, OBJECTIVES_LIST : Dictionary):
	for property in QUEST.keys():
		self.set(property, QUEST[property]);
	self.set_objectives(OBJECTIVES_LIST);
	self.set_contractor_icon(load(contractor_icon_path));


#########################################
#		OBJECTIVES LIST MANAGEMENT		#
#########################################

# Adds a new objective to the list
func add_objective(new_objective : Objective) -> void:
	if (self.objectives.has(new_objective) == false):
		self.objectives.append(new_objective);


# Returns an objective given its index
func get_objective(objective_index : int) -> Objective:
	return self.objectives[objective_index];


# Removes an objective given its index
func remove_objective(objective_index : int) -> void:
	if (self.objective_index < objectives.size()):
		self.Objectives.remove(objective_index);


# Returns an objective name given its index
func get_objective_name(objective_index : int) -> String:
	return self.Objectives[objective_index].get_name();


# Returns an objective description given its index
func get_objective_description(objective_index : int) -> String:
	return self.objectives[objective_index].get_description();


# Returns an objective location given its index
func get_objective_location(objective_index : int) -> String:
	return self.objectives[objective_index].get_location();


# Sets an objective as complete
func complete_objective(objective_index : int) -> void:
	self.objectives[objective_index].set_completed();
	self.completed_objectives += 1;
	if (completed_objectives == self.get_objectives_number()):
		self.set_completed();


# Returns the total number of objectives
func get_objectives_number() -> int:
	return self.objectives.size();


# Sets an objective as failed
func fail_objective(objective_index : int) -> void:
	self.objectives[objective_index].set_failed();
	if (self.is_objective_optional(objective_index) == false):
		self.set_failed();


# Sets an objective as active
func activate_objective(objective_index : int) -> void:
	self.objectives[objective_index].set_active();


# Sets an objective as inactive
func inactivate_objective(objective_index : int) -> void:
	self.objectives[objective_index].set_inactive();


# Returns true if the objective is complete
func is_objective_completed(objective_index : int) -> bool:
	return self.objectives[objective_index].is_completed();


# Returns true if the objective is failed
func is_objective_failed(objective_index : int) -> bool:
	return self.objectives[objective_index].is_failed();


# Returns true if the objective is active
func is_objective_active(objective_index : int) -> bool:
	return self.objectives[objective_index].is_active();


# Returns true if the objective is inactive
func is_objective_inactive(objective_index : int) -> bool:
	return self.objectives[objective_index].is_inactive();


# Returns true if the objective is optional
func is_objective_optional(objective_index : int) -> bool:
	return self.objectives[objective_index].is_optional();


# Returns objective level
func get_objective_level(objective_index : int) -> bool:
	return self.objectives[objective_index].get_level();


# Sets if the quest is started or not
func set_started(started : bool = true) -> void:
	self.started = started;


# Returns true if the quest is started
func is_started() -> bool:
	return self.started;


#################################
#		SETTERS AND GETTERS		#
#################################

# Sets the quest state
func set_state(new_state : String) -> void:
	self.quest_state = new_state;


# Returns the quest state
func get_state() -> String:
	return self.quest_state;


# Sets the quest contractor's name
func set_contractor_name(new_name : String) -> void:
	self.quest_contractor_name = new_name;


# Returns the quest contractor's name
func get_contractor_name() -> String:
	return self.quest_contractor_name;


# Sets the quest contractor's location
func set_contractor_location(new_loaction : String) -> void:
	self.quest_contractor_location = new_loaction;


# Returns the quest contractor's location
func get_contractor_location() -> String:
	return self.quest_contractor_location;


# Sets the quest contractor icon path
func set_contractor_icon_path(new_icon : String) -> void:
	var icon = load(new_icon);
	if (new_icon != null):
		self.quest_contractor_icon_path = new_icon;
		self.set_contractor_icon(icon);


# Sets the contractor icon
func set_contractor_icon(icon : Resource) -> void:
	self.quest_contractor_icon = icon;

# Returns contractor icon
func get_contractor_icon() -> Resource:
	return self.quest_contractor_icon;
	

# Returns the quest contractor icon path
func get_contractor_icon_path() -> String:
	return self.quest_contractor_icon_path;


# Sets the quest level
func set_level(new_level : int) -> void:
	self.quest_level = new_level;


# Returns the quest level
func get_level() -> int:
	return self.quest_level;


# Sets the quest type
func set_type(type : String) -> void:
	self.quest_type = type;


# Returns the quest type
func get_type() -> String:
	return self.quest_type;


# Returns the quest objectives
func get_objectives() -> Array:
	return self.Objectives;


# Sets the quest objectives
func set_objectives(objectives_data : Dictionary) -> void:
	for objective_name in objectives_data.keys():
		if (objectives_data[objective_name]["quest_name"] == self.get_name()):
			var new_objective = Objective.new(objectives_data[objective_name]);
			if (new_objective != null):
				self.add_objective(new_objective);
			else:
				print_debug("An error occurred while trying to create objective " + objectives_data[objective_name]["name"]);
	self.sort_objectives();
	self.set_max_level();

# Returns true if the quest is complete
func is_completed() -> bool:
	return self.quest_state == "Completed";


# Returns true if the quest is failed
func is_failed() -> bool:
	return self.quest_state == "Failed";


# Returns true if the quest is active
func is_active() -> bool:
	return self.quest_state == "Active";


# Returns true if the quest is inactive
func is_inactive() -> bool:
	return self.quest_state == "Inactive";


# Sets the quest as completed
func set_completed() -> void:
	self.set_state("Completed");


# Sets the quest as failed
func set_failed() -> void:
	self.set_state("Failed");


# Sets the quest as active
func set_active() -> void:
	self.set_state("Active");


# Sets the quest as inactive
func set_inactive() -> void:
	self.set_state("Inactive");


# Returns the default path to the quest data
func get_quests_data_path() -> String:
	return self.QUESTS_DATA_PATH;


#########################
#	INTERNAL METHODS	#
#########################

# Sorts objectives using the objective numbers
func sort_objectives() -> void:
	objectives.sort_custom(self, "sort_by_objective_number");


# Custom sort function using the objective numbers
func sort_by_objective_number(obj_a : Objective, obj_b : Objective) -> bool:
	if (obj_a.get_number() < obj_b.get_number()):
		return true;
	return false;
	
	
func set_max_level() -> void:
	var max_level = 0;
	for objective in objectives:
		max_level = max(objective.get_level(), max_level);
	self.set_level(max_level);


func set_avarage_level() -> void:
	var level_sum = 0;
	for objective in objectives:
		level_sum += objective.get_level();
	var avarage_level = level_sum / objectives.size();
	self.set_level(avarage_level)
