extends Entry
class_name Objective

# Variables

var quest_name;
var location;
var level : int;
var optional : bool;
var state : String;
var number : int;

###########

# Objective constructor
func _init(OBJECTIVE : Dictionary):
	for property in OBJECTIVE.keys():
		self.set(property, OBJECTIVE[property]);


#################################
#		SETTERS AND GETTERS		#
#################################

# Returns the objective level
func get_level() -> int:
	return self.level;

# Sets the objective level
func set_level(new_level : int) -> void:
	self.level = new_level;

# Returns the objective location
func get_location() -> String:
	return self.location;

# Sets the objective location
func set_location(location : String) -> void:
	self.location = location;

# Sets the objective state
func set_state(state : String) -> void:
	self.state = state;

# Returns the objective_state
func get_state() -> String:
	return self.state;

# Returns true if the objective is complete
func is_completed() -> bool:
	return self.state == "Completed";

# Returns true if the objective is failed
func is_failed() -> bool:
	return self.state == "Failed";

# Returns true if the objective is active
func is_active() -> bool:
	return self.state == "Active";

# Returns true if the objective is inactive
func is_inactive() -> bool:
	return self.state == "Inactive";
	
# Sets the objective as completed
func set_completed() -> void:
	self.set_state("Completed");

# Sets the objective as failed
func set_failed() -> void:
	self.set_state("Failed");

# Sets the objective as active
func set_active() -> void:
	self.set_state("Active");

# Sets the objective as inactive
func set_inactive() -> void:
	self.set_state("Inactive");

# Sets if the objective is optional
func set_optional(is_optional : bool) -> void:
	self.optional = is_optional;

# Returns true if the objective is optional
func is_optional() -> bool:
	return self.optional;

# Returns the default path to the objective data
func get_objectives_data_path() -> String:
	return self.OBJECTIVES_DATA_PATH;

# Sets the name of the related quest
func set_quest_name(name : String) -> void:
	self.quest_name = name;

# Returns the name of the related quest
func get_quest_name() -> String:
	return self.quest_name;

# Returns the objective number
func get_number() -> int:
	return self.number;

# Sets the objective number
func set_number(num : int) -> void:
	self.number = num;
	
