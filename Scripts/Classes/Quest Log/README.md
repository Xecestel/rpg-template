# Quest Log
## Version 0.5
## © Xecestel
## Licensed Under MIT License (see below)

## Overview
This class is a quest log mainly made for role-playing videogames, to store and manage quests and objectives.  
It's made as a subclass of the [Log Base Class](https://gitlab.com/Xecestel/log-base), so **in order to make it work it is required to place both repositories inside your Godot project.**  
Apart from the Log Manager, this repository contains three scripts:
- QuestLog.gd, containing the methods and variables of the QuestLog class, to handle and manage quests and objectives;
- Quest.gd, containing the methods and variables of the Quest class (Entry subclass);
- Objective.gd, containing the methods and variables of the Objective class;

## How does it work
### Configuration
The Quest and Objective classes contain all the information about quests and objectives. Name, description, state (`Active`, `Complete`, `Failed`...), location of the objective, even information about the contractor of the quest (aka the NPC that gave the quest). However, almost any of these info are optional, leaving as mandatory only the names of the objectives and quests.  
To understand what you need to do to make these classes work, you have to understand how they were programmed to work in the first place.  
The `_init` methods, the object constructors, require the name of the quest/objective. What do they do with it? They look for a file that matches this name inside two particular folders in your project directory. By default, the folders are set as `Data/QuestsData/` and `Data/QuestsData/ObjectivesData/`, but you can change them as you wish, as they're saved as constants inside the scripts. Once the scripts find the matching file, they take all the information they need to create a new object. So what you need to do is just create the file and pass the file name.  
To help you create the file, I placed two templates inside this repository. Copy-paste them and edit them as you wish. You can delete the info you don't want to use.  

### Methods and variables
The Quests and Objectives script can now be used to access these information, but this is not their intended use: they are to be accessed through the QuestLog class. This class contains all the methods you need to access and edit all the quests and objectives information.  
####TOFINISH

## Notes
This class extends the [Log Base Class](https://gitlab.com/Xecestel/log-base) class.  

This class was originally made as part of the [RPG Template project](https://gitlab.com/Xecestel/rpg-template).  

## Licenses
Copyright © 2019-2020 Celeste Privitera  

This software is an open source project licensed under MIT License. Check the LICENSE file for more info.

## Changelog
### Version 0.1
- The class was created.

### Version 0.2
- Removed submodule

### Version 0.3
- Added QuestTemplate and ObjectiveTemplate files
- Updated README

### Version 0.4
- Redesigned log creation: now the module only needs two file (one for the quests and one for the objectives) to fill the log instead than two directories. This will improve performances.
- Updated LogBase and Entry classes: now the entries are saved as a dictionary to make the access easier
- Bug fixes & improved code readability

### Version 0.5
- Added methods to calculate quests level
- Bug fixes
