class_name CharacterBase

# Variables

var Name : String;
var Description : String = "";
var Face_Path : String = "";
var Portrait_Path : String = "";

var face_texture : Texture;
var portrait : PackedScene;

###########

# Constructor for a generic character
func _init(Character_Data : Dictionary) -> void:
	if (Character_Data != null && Character_Data.empty() == false):
		for property in Character_Data.keys():
			self.set(property, Character_Data[property]);


#########################
#	SETTERS & GETTERS	#
#########################

# Returns character name
func get_name() -> String:
	return Name;


# Sets character name
func set_name(char_name : String) -> void:
	Name = char_name;


# Sets a new path for the face texture
func set_face_path(texture_path : String) -> void:
	Face_Path = texture_path;
	self.set_face();


# Returns the path of the face texture
func get_face_path() -> String:
	return Face_Path;


# Sets a new Resource to the face texture
func set_face() -> void:
	if (Face_Path == ""):
		return;
	face_texture = load(Face_Path);
	if (face_texture == null):
		print_debug("Unable to load resource from path: " + Face_Path);
		return;


# Returns the Resource variable of the face texture
func get_face_texture() -> Resource:
	return face_texture;


# Sets a new resource path for the portrait
func set_portrait_path(scene_path : String) -> void:
	Portrait_Path = scene_path;
	self.set_portrait();


# Sets a new Resource variable for the portrait
func set_portrait() -> void:
	if (Portrait_Path == ""):
		return;
	portrait = load(Portrait_Path);
	if (portrait == null):
		print_debug("Unable to load resource from path: " + Portrait_Path);


# Returns the path of the portrait scene
func get_portrait_path() -> String:
	return Portrait_Path;


# Retrusn the portrait Resource
func get_portrait() -> PackedScene:
	return portrait;


# Sets the character's description
func set_description(desc : String = "") -> void:
	Description = desc;


# Return the character's description
func get_description() -> String:
	return Description;
