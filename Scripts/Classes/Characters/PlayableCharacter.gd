extends BattleCharacter
class_name PlayableCharacter

# Variables

var Exp : int = 0;

###########

# Called when the node enters the scene tree for the first time.
func _init(Character_Data : Dictionary).(Character_Data) -> void:
	if (Character_Data != null && Character_Data.empty() == false):
		for property in Character_Data.keys():
			self.set(property, Character_Data[property]);


#########################
#	SETTERS & GETTERS	#
#########################

# Returns the character experience points
func get_exp() -> int:
	return Exp;

# Returns the total experience points needed to reach next level
#func get_exp_to_next() -> int:
#	return Exp_to_Next;

# Return the number of experience points left to reach next level
#func get_left_exp() -> int:
#	return Exp_to_Next - Exp;


func give_exp(exp_points : int) -> void:
	Exp += exp_points;
