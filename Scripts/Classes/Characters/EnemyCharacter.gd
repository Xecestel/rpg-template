extends BattleCharacter
class_name EnemyCharacter


# Variables

var Loot : Dictionary;

###########


# Called when the node enters the scene tree for the first time.
func _init(Character_Data : Dictionary).(Character_Data):
	if (Character_Data != null && Character_Data.empty() == false):
		for property in Character_Data.keys():
			self.set(property, Character_Data[property]);


#########################
#	SETTERS & GETTERS	#
#########################

# Sets the loot for this character
func set_loot(loot : Dictionary) -> void:
	Loot = loot;

# Returns the enemy loot
func get_loot() -> Dictionary:
	return Loot;
