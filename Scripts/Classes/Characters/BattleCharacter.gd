extends CharacterBase
class_name BattleCharacter

# Variables

var Equipment : Dictionary;
var Level : int;
var Stats : Dictionary;
var Equip_Categories : Array;
var Class_Name : String;

############

# Signals

signal new_level(level);
signal stat_incremented(stat, increment);

##########


#Methods#

func _init(Character_Data : Dictionary).(Character_Data) -> void:
	if (Character_Data != null && Character_Data.empty() == false):
		for property in Character_Data.keys():
			self.set(property, Character_Data[property]);
	update_class();
	manage_level_up();



#####################
#		SETTERS		#
#####################

# Sets a given dictionary as the stats dictionary
func set_stats(stats : Dictionary) -> void:
	Stats = stats;


# Sets a given value on a specific stat
func set_stat(stat : String, value : int) -> void:
	if (Stats.has(stat)):
		Stats[stat] = value;


# Adds a given value on a specific stat
func add_stat(stat : String, value : int) -> void:
	if (Stats.has(stat)):
		Stats[stat] += value;


# Adds a set amount of levels to the character
func gain_level(how_many_levels : int = 1) -> void:
	Level += how_many_levels;
	emit_signal("new_level", Level);
	manage_level_up();


# Sets a specific level
func set_level(level : int) -> void:
	Level = level;
	emit_signal("new_level", Level);
	manage_level_up();


# Sets a given dictionary as the equipment dictionary
func set_equipment(equipment : Dictionary) -> void:
	Equipment = equipment;


# Sets a given item on a specific equipment slot
# Returns true if the item has been equipped correctly
func set_equip_item(equip_slot : String, item_name : String) -> bool:
	if (Equipment.has(equip_slot) == false):
		print_debug("Unable to find equipment slot: " + equip_slot);
		return false;
	
	var item = GlobalVariables.get_item(item_name);
	if (item == null):
		print_debug("Unable to find item: " + item_name);
		return false;
	
	if (item is ItemEquippable == false):
		print_debug("The selected item is not an equippable item")
		return false;
	
	if (item.has_equip_slot(equip_slot) == false):
		print_debug("The selected item can't be equipped on the slot " + equip_slot);
		return false;
	
	if (self.is_item_equippable(item) == false):
		print_debug("The selected character can't equip that item");
		return false;

	Equipment[equip_slot] = item_name;
	return true;


# Sets the selected equipment slot as empty
func unequip_item(equip_slot : String) -> void:
	if (Equipment.has(equip_slot)):
		Equipment[equip_slot] = "";
	else:
		print_debug("Unable to find equipment slot: " + equip_slot);


# Sets a given categories array to the equip categories
func set_equip_categories(categories : Array) -> void:
	Equip_Categories = categories;


# Returns equip categories array
func get_equip_categories() -> Array:
	return Equip_Categories;


# Returns the name of the character class
func get_class_name() -> String:
	return Class_Name;


#func get_class():
#	return GlobalVariables.get_class(Class);


# Sets a given class as a character class
# If increment_stats is true, simulates a level up to the new class
# If reset_stats is true, resets the character stats to the new class base stats
func set_class(new_class : String, increment_stats : bool = false) -> void:
	Class_Name = new_class;
	update_class(increment_stats);


# Sets a given class passed by a String as a character class
# If increment_stats is true, simulates a level up to the new class
# If reset_stats is true, resets the character stats to the new class base stats
func set_class_as_string(new_class : String, increment_stats : bool = false) -> void:
	var character_class_index = GlobalVariables.get_class_index(new_class);
	set_class(character_class_index, increment_stats);



#####################
#		GETTERS		#
#####################

# Returns stats as dictionary
func get_stats() -> Dictionary:
	return Stats;


# Returs stats as string
func get_stats_as_string() -> String:
	return str(Stats);
	
	
# Returns specific stat (argument is the stat name)
func get_stat(stat : String) -> int:
	return Stats.get(stat, -1);


# Returns true if the given stat exists
func stat_exists(stat : String) -> bool:
	return Stats.has(stat);
	
	
# Returns equipment as dictionary
func get_equipment() -> Dictionary:
	return Equipment;
	
	
# Returns equipment as string
func get_equipment_as_string() -> String:
	return str(Equipment);

	
# Returns specific equipped item (equip_slot is the equip slot name)
func get_equip_item_name(equip_slot : String) -> String:
	return Equipment.get(equip_slot, null);


func get_equip_item(equip_slot : String):
	GlobalVariables.get_item(self.get_equip_item_name(equip_slot));
	
	
# Returns true if the given equip slot exists
func equip_slot_exists(equip_slot : String) -> bool:
	return Equipment.has(equip_slot);
	
	
# Returns true if the given equip slot has an item equipped
func is_equip_slot_equipped(equip_slot : String) -> bool:
	return get_equip_item(equip_slot) != "";


# Returns true if the given item category can be equipped
func is_category_equippable(equip_category : String) -> bool:
	var equippable = Equip_Categories.has(equip_category);
	if (Class_Name != "" && Class_Name != null):
		var class_data = retrieve_class_data();
		if (class_data != null):
			equippable = equippable && class_data.has_equip_category(equip_category);
	return equippable;


# Returns true if given item is equippable
func is_item_equippable(item : ItemEquippable) -> bool:
	var equip_category = item.get_category();
	return is_category_equippable(equip_category);


# Returns true if the given item passed by name is equippable
func is_item_equippable_by_name(item_name : String) -> bool:
	var item = GlobalVariables.get_item(item_name);
	return is_item_equippable(item);


#################################
#		INTERNAL METHODS		#
#################################

# Handles level ups
func manage_level_up() -> void:
	var max_level = GlobalVariables.get_max_level();
	var class_data : Class = GlobalVariables.get_character_class(Class_Name);
	if (class_data == null):
		print_debug("Unable to find class: " + Class_Name);
		return;
	var stats_increment = class_data.get_stats_increment();
	var l = float(Level/max_level);
	for stat in Stats.keys():
		var increment_curve = stats_increment[stat];
		var from = Stats[stat];
		var to = increment_stat(increment_curve, l);
		emit_signal("stat_incremented", stat, to - from);
		Stats[stat] = to;
#	Stats["ATK"] = 
#	var i : float = 3/100.0
#	print_debug(float(i))
#	var interpolation = Vector2(0, 46).linear_interpolate(Vector2(100, curve["Finish"]), i)
#	print_debug(-1*(curve["Finish"]-curve["Start"])/(100-1) + curve["Start"])
#	print_debug(interpolation.round())
#	print_debug("+" + str(round(interpolation.y)-54))
#	print_debug(floor(interpolation.y) * ease(interpolation.y/450, curve["Ease"]))
#	increment_stats(level_increment);
#	Stats["Level"] = Level;


# Handles class change
func update_class(increment_stats : bool = false) -> void:
	var class_data = GlobalVariables.get_character_class(Class_Name);
	
	if (class_data == null):
		print_debug("Unable to read class settings");
		return;
	
	if (increment_stats):
		set_level(Level);



#################################
#		INTERNAL METHODS		#
#################################

# Given an increment curve and an increment level, returns the stat value
# at the given level
func increment_stat(increment_curve : Dictionary, increment : int) -> int:
	var max_level = GlobalVariables.get_max_level();
	var v_from = Vector2(0, increment_curve["Start"]);
	var v_to = Vector2(max_level, increment_curve["Finish"]);
	var interpolation = v_from.linear_interpolate(v_to, increment);
	return interpolation;


# Retrieves class data from GlobalVariables
func retrieve_class_data() -> Class:
	var class_data = GlobalVariables.get_character_class(Class_Name);
	if (class_data != null):
		print_debug("Unable to retrieve data for class " + Class_Name);
	return class_data;
