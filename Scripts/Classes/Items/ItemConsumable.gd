extends Item
class_name ItemConsumable

# Variables

var Target : String = "Self";
var Effect : Dictionary = {}
var Temporary : bool = false;
var From_Battle : bool = true;
var From_Inventory : bool = true;

##########

#Constructor for a Consumable Item
func _init(Item_Data : Dictionary).(Item_Data) -> void:
	if (Item_Data != null && Item_Data.empty() == false):
		for property in Item_Data.keys():
			self.set(property, Item_Data[property]);


#########################
#	SETTERS & GETTERS	#
#########################

# Return the complete effects dictionary
func get_effect_list() -> Dictionary:
	return Effect;


# Returns the effect the item have on a specific stat
func get_effect_on_stat(stat : String, default : int = -1) -> int:
	return Effect.get(stat, default);


# Returns true if the effects dictionary has a specific stat
func has_effect_on_stat(stat : String) -> bool:
	return Effect.has(stat);


#Returns the item target
func get_target() -> String:
	return Target;


#Sets the item target
func set_target(target : String) -> void:
	Target = target;


#Sets the effects dictionary
func set_effect_from_dictionary(effect : Dictionary) -> void:
	Effect = effect;


#Sets a new effect on a specific stat
func set_effect_on_stat(stat : String, effect : int) -> void:
	Effect[stat] = effect;


# Sets if the effects are temporary
func set_temporary(temporary : bool = true) -> void:
	Temporary = temporary;


# Returns true if the item effects are temporary
func is_temporary() -> bool:
	return Temporary;


# Sets if the item can be used from battle
func set_from_battle(enable : bool) -> void:
	From_Battle = enable;


# Returns true if the item can be used from battle
func get_from_battle() -> bool:
	return From_Battle;


# Sets if the item can be used from inventory
func set_from_inventory(enable : bool) -> void:
	From_Inventory = enable;


# Returns true if the item can be used from inventory
func get_from_inventory() -> bool:
	return From_Inventory;
