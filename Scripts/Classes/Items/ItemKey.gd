extends Item
class_name ItemKey


# Variables

var Usable : bool = false;

############


# Constructor for a Key Item
func _init(Item_Data : Dictionary).(Item_Data) -> void:
	if (Item_Data != null && Item_Data.empty() == false):
		for property in Item_Data.keys():
			self.set(property, Item_Data[property]);


# Returns true if the item can be used from inventory
func is_usable() -> bool:
	return Usable;


# Sets if the item can be used from inventory
func set_usable(usable : bool = true) -> void:
	Usable = usable;
