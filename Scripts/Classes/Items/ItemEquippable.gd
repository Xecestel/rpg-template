extends Item
class_name ItemEquippable

# Variables

var Category : String;
var Equip_Slots : Array;
var Effect : Dictionary = {};
var Durability : int = 100;
var Max_Durability : int = 100;

###########

# Constructor for a Consumable Item
func _init(Item_Data : Dictionary).(Item_Data) -> void:
	if (Item_Data != null && Item_Data.empty() == false):
		for property in Item_Data.keys():
			self.set(property, Item_Data[property]);


# Sets item's equip category
func set_category(category : String) -> void:
	Category = category;


# Sets item's equippable body part
func set_equip_slots(slots : Array) -> void:
	Equip_Slots = slots;


# Sets the effects dictionary
func set_effects_from_dictionary(effects : Dictionary) -> void:
	Effect = effects;


# Sets a new effect on a specific stat
func set_effect_on_stat(stat : String, effect : int) -> void:
	Effect[stat] = effect;


# Returns item's equip category
func get_category() -> String:
	return Category;


# Return the complete effects dictionary
func get_effects_list() -> Dictionary:
	return Effect;


# Returns the effect the item have on a specific stat
func get_effect_on_stat(stat : String, default : int = -1) -> int:
	return Effect.get(stat, default);


# Returns true if the effects dictionary has a specific stat
func has_effect_on_stat(stat : String) -> bool:
	return Effect.has(stat);


# Sets the item's durability
func set_durability(durability : int) -> void:
	Durability = durability;


# Returns the item's durability
func get_durability() -> int:
	return Durability;


# Returns if the item is broken
func is_broken() -> bool:
	return Durability == 0;


# Returns item's type
func get_type() -> String:
	return Type;


# Returns item's equippable body part
func get_equip_slot() -> Array:
	return Equip_Slots;


# Returns true if the item has the given equip slot
func has_equip_slot(equip_slot : String) -> bool:
	return Equip_Slots.has(equip_slot);


# Returns item's max durability
func get_max_durability() -> int:
	return Max_Durability;


# Sets item's max durability
func set_max_durability(durability : int) -> void:
	Max_Durability = durability;


# Subtract a certain amount of durability to the item
func subtract_durability(dur_to_subtract : int) -> bool:
	Durability = max(Durability - dur_to_subtract, 0);
	return is_broken();


# Adds a certain amount of durability to the item
func add_durability(dur_to_add : int) -> void:
	Durability = min(Durability + dur_to_add, Max_Durability);


# Completely zeroes item's durability
func break_item() -> void:
	set_durability(0);


# Fully restores item durability
func repair_item() -> void:
	set_durability(get_max_durability());
