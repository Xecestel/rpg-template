class_name Item

# Variables

var Name : String;
var Description : String;
var Weight : float = 0.0;
var Removable : bool = true;
var Icon_Path : String = "";
var Sprite_Path : String = "";
var Type : String = "Consumable";
var Cost : int = 0;

var icon_texture : Texture;
var sprite_texture : Texture;

###########

# Constructor for a generic Item
func _init(Item_Data : Dictionary) -> void:
	if (Item_Data != null && Item_Data.empty() == false):
		for property in Item_Data.keys():
			self.set(property, Item_Data[property]);
	self.set_icon_texture();
	self.set_sprite_texture();


# Returns the item's name
func get_name() -> String:
	return Name;


# Returns the item's description
func get_description() -> String:
	return Description;


# Sets the item's name
func set_name(name : String) -> void:
	Name = name;


# Sets the item's description
func set_description(description : String) -> void:
	Description = description;


# Returns the item weight
func get_weight() -> float:
	return Weight;


# Sets the item's weight
func set_weight(weight : float) -> void:
	Weight = weight;


# Returns true if the item can be sold
func is_sellable() -> bool:
	return Cost > 0;


# Sets if the item can be removed from inventory
func set_removable(enable : bool) -> void:
	Removable = enable;


# Returns true if the item can be removed from inventory
func is_removable() -> bool:
	return Removable;


# Returns the loaded texture of the item icon
func get_icon_texture() -> Resource:
	return icon_texture;


# Returns the path of the item icon texture
func get_icon_path() -> String:
	return Icon_Path;


# Sets a loaded resource as an icon texture
func set_icon_texture() -> void:
	if (Icon_Path == ""):
		return;
	icon_texture = load(Icon_Path);
	if (icon_texture == null):
		print_debug("Unable to load resource from path: " + Icon_Path);


# Sets a texture path as an icon path 
func set_icon_path(path : String) -> void:
	Icon_Path = path;
	self.set_icon_texture();


# Returns the loaded texture of the item sprite
func get_sprite_texture() -> Resource:
	return sprite_texture;


# Returns the path of the item sprite texture
func get_sprite_path() -> String:
	return Sprite_Path;


# Sets a loaded resource as a sprite texture
func set_sprite_texture() -> void:
	if (Sprite_Path == ""):
		return;
	sprite_texture = load(Sprite_Path);


# Sets a texture path as a sprite path 
func set_sprite_path(path : String) -> void:
	Sprite_Path = path;
	self.set_sprite_texture();


# Returns item category
func get_type() -> String:
	return Type;


# Sets Item category
func set_type(type : String) -> void:
	Type = type;


# Sets item cost
func set_cost(cost : int) -> void:
	Cost = cost;


# Returns item cost
func get_cost() -> int:
	return Cost;
