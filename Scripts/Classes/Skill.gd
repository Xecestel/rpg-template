class_name Skill


# Variables

var Name : String;
var Description : String = "";
var Icon_Path : String;
var Type : String;
var Cost : Dictionary;
var Target : String;
var From_Battle : bool = false;
var From_Menu : bool = false;
var Stats : Dictionary = {
	"Success_Rate" : 100,
	"Repeat" : 1,
	};
var Animation_Name : String;
var Message : String = "/U[Name] uses *!";
var Requirements : Dictionary = {
	"Weapon_Type" : [ null, "" ],
	};
var Damage_Options : Dictionary = {
	"Formula" : "/U[Stats['ATK']] * 4 - /T[Stat['Def']] * 2",
	"Crits" : true,
	"Variance" : 20,
	};
var Effects : Dictionary = {
	"States" : [{"Normal Attack" : 100}],
	};

var icon : Texture = null;

############


# Constructor for a Skill Object
func _init(Skill_Data : Dictionary) -> void:
	if (Skill_Data != null && Skill_Data.empty() == false):
		for property in Skill_Data.keys():
			self.set(property, Skill_Data[property]);
	self.set_icon();


#########################
#	SETTERS & GETTERS	#
#########################

# Sets a new name for the Skill
func set_name(name : String) -> void:
	Name = name;


# Returns Skill name
func get_name() -> String:
	return Name;


# Sets a new description for the Skill
func set_description (description : String) -> void:
	Description = description;


# Returns the Skill description
func get_description() -> String:
	return Description;


# Sets a new path for the Skill icon
func set_icon_path(path : String) -> void:
	Icon_Path = path;
	self.set_icon();


# Sets a new icon for the Skill
func set_icon() -> void:
	var res = load(Icon_Path);
	if (res):
		icon = res;
	else:
		print_debug("Unable to load icon from path: " + Icon_Path);


# Returns icon path
func get_icon_path() -> String:
	return Icon_Path;


# Returns Skill icon
func get_icon() -> Texture:
	return icon;


# Sets the Skill type
func set_type(type : String) -> void:
	Type = type;


# Returns the Skill type
func get_type() -> String:
	return Type;


# Sets the Skill cost
func set_cost(cost : Dictionary) -> void:
	Cost = cost;


# Adds a new cost to the Skill
# If the cost already exists, updates it
func add_cost(id : String, value : int) -> void:
	Cost[id] = value;


# Removes a cost from the Cost Dictionary and returns it
# Returns -1 if the cost was not found
func erase_cost(id : String) -> int:
	var removed_cost = Cost.get(id, -1);
	Cost.erase(id);
	return removed_cost;


# Returns a specific cost of the skill
# Returns -1 if the cost was not found
func get_cost_by_id(id : String) -> int:
	return Cost.get(id, -1);


# Returns Cost Dictionary
func get_cost() -> Dictionary:
	return Cost;


# Sets a target for the Skill
func set_target(target : String) -> void:
	Target = target;


# Returns the Skill target
func get_target() -> String:
	return Target;


# Enables Skill usage from batle
func enable_from_battle(enabled : bool = true) -> void:
	From_Battle = enabled;


# Returns true if the Skill can be used from battle
func is_from_battle_enabled() -> bool:
	return From_Battle;


# Enables Skill usage from menu
func enable_from_menu(enabled : bool = true) -> void:
	From_Menu = enabled;


# Returns true if the Skill can be used from menu
func is_from_menu_enabled() -> bool:
	return From_Menu;


# Sets the Skill stats
func set_stats(stats : Dictionary) -> void:
	Stats = stats;


# Adds a new stat
# If the stat already exists, updates it
func add_stat(id : String, value) -> void:
	Stats[id] = value


# Removes a stat from the Stats dictionary and returns it
# Returns null if the stat was not found
func erase_stat(id : String):
	var removed_stat = Stats.get(id, null);
	Stats.erase(id);
	return removed_stat;


# Returns a specific stat from the Stats dictionary
# Returns null if the stat was not found
func get_stat(id : String):
	return Stats.get(id, null);


# Returns the Skill stats
func get_stats() -> Dictionary:
	return Stats;


# Sets the Skill animation
func set_animation(animation_name : String) -> void:
	Animation_Name = animation_name;


# Returns the Skill animation
func get_animation() -> String:
	return Animation_Name;


# Sets the Skill battle message
func set_message(message : String) -> void:
	Message = message;


# Returns the Skill message
func get_message() -> String:
	return Message;


# Sets the skill requirements
func set_requirements(requirements : Dictionary) -> void:
	Requirements = requirements;


# Adds a new requirement list to the skill
# If overwrite is true, it overwrites the previous list
func add_requirements(id : String, value : Array, overwrite : bool = false) -> void:
	var requirement : Array;
	if (overwrite):
		requirement = value;
	else:
		requirement = Requirements.get(id, []);
		requirement = requirement + value;
	Requirements[id] = requirement;


# Removes a Skill requirement and returns it
# Returns an empty array if the requirement was not found
func erase_requirements(id : String) -> Array:
	var removed_requirements = Requirements.get(id, []);
	Requirements.erase(id);
	return removed_requirements;


# Returns a specific requirement list from the dictionary
# Returns an empty array if the requirement was not found
func get_requirement(id : String) -> Array:
	return Requirements.get(id, []);


# Returns the Requirements dictionary
func get_requirements() -> Dictionary:
	return Requirements;


# Sets the Damage_Options dictionary
func set_damage_options(options : Dictionary) -> void:
	Damage_Options = options;


# Adds a new option to the Damage_Options dictionary
# If it already exists, it overwrites it
func add_damage_option(id : String, option) -> void:
	Damage_Options[id] = option;


# Removes a Damage Option and returns it
# Returns null if the option was not found
func erase_damage_option(id : String):
	var removed_option = Damage_Options.get(id, null);
	Damage_Options.erase(id);
	return removed_option;


# Returns a damage option from the dictionary
# Returns null if the option was not found
func get_damage_option(id : String):
	return Damage_Options.get(id, null);


# Returns the Damage Options dictionary
func get_damage_options() -> Dictionary:
	return Damage_Options;


# Sets the Skill effects
func set_effects(effects : Dictionary) -> void:
	Effects = effects;


# Adds a new effects list to the skill
# If overwrite is true, it overwrites the previous list
func add_effects(id : String, value : Array, overwrite : bool = false) -> void:
	var effect : Array;
	if (overwrite):
		effect = value;
	else:
		effect = Effects.get(id, []);
		effect = effect + value;
	Effects[id] = effect;


# Removes an effect list and returns it
# Returns an empty array if it was not found
func erase_effect(id : String) -> Array:
	var removed_effect = Effects.get(id, []);
	Effects.erase(id);
	return removed_effect;


# Returns an effect from the dictionary
# Returns an empty array if it was not found
func get_effect(id : String) -> Array:
	return Effects.get(id, []);


# Returns the Skill Effects dictionary
func get_effects() -> Dictionary:
	return Effects;
