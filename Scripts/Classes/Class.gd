class_name Class


# Variables

var Name : String
var Description : String
var Prerequisites : Dictionary = {
		"Class" : "",
		"LVL"	: 0,
		"Learned Skills" : [],
}
var Equip_Categories : Array
var Stats_Increment : Dictionary = {
	"Stat" : {
		"Start" : 0, "Finish" : 50,
		},
}
var Skills : Dictionary = {
	"Level" : [],
}

#############

# Constructor for the class
func _init(Class_Data : Dictionary) -> void:
	if Class_Data != null and not Class_Data.empty():
		for property in Class_Data.keys():
			self.set(property, Class_Data[property])
		self.normalize_stats_increment_curves();



#########################
#	SETTERS & GETTTERS	#
#########################

# Sets a new name for the class
func set_name(name : String) -> void:
	Name = name


# Returns the class name
func get_name() -> String:
	return Name


# Sets a new description for the class
func set_description(description : String) -> void:
	Description = description


# Returns the class description
func get_description() -> String:
	return Description


# Sets a new dictionary of prerequisites for the class
func set_prerequisites(prerequisites : Dictionary) -> void:
	Prerequisites = prerequisites


# Set a new prerequisite for the class
func set_prerequisite(id : String, value) -> void:
	Prerequisites[id] = value


# Removes a prerequisite from the class given its id and returns it
func remove_prerequisite(id : String):
	var removed_prerequisite = Prerequisites[id];
	Prerequisites.erase(id)
	return removed_prerequisite;


# Clears the prerequisites dictionary and returns it
func clear_prerequisites() -> Dictionary:
	var prerequisites = Prerequisites;
	Prerequisites.clear();
	return prerequisites;


# Returns the class prerequisites
func get_prerequisites() -> Dictionary:
	return Prerequisites


# Returns a specific prerequisite given its id
func get_prerequisite(id : String, default = null):
	return Prerequisites.get(id, default)


# Returns true if the Prerequisites dictionary is empty
func is_prerequisites_empty() -> bool:
	return Prerequisites.empty()


# Returns true if the Prerequisites dictionary has the given key
func has_prerequisite(id : String) -> bool:
	return Prerequisites.has(id)


# Sets a new list of equipment categories
func set_equip_categories(list : Array) -> void:
	Equip_Categories = list


# Adds a new equipment category to the list
func add_equip_category(category : String) -> void:
	Equip_Categories.append(category)


# Removes an equipment category from the list given its name
func erase_equip_category(category : String) -> void:
	Equip_Categories.erase(category)


# Removes an equipment category from the list given its index and returns it
func remove_equip_category(index : int) -> String:
	var removed_category = Equip_Categories[index]
	Equip_Categories.remove(index)
	return removed_category


# Clears the equipment category list and returns it
func clear_equip_categories() -> Array:
	var categories = Equip_Categories
	Equip_Categories.clear()
	return categories


# Returns the list of equipment categories
func get_equip_categories() -> Array:
	return Equip_Categories


# Gets an equipment category given its index
func get_equip_category(index : int) -> String:
	return Equip_Categories[index]


# Returns true if the Equip_Categories list is empty
func is_equip_category_empty() -> bool:
	return Equip_Categories.empty()


# Returns true if the Equip_Categories list has the given element
func has_equip_category(category : String) -> bool:
	return Equip_Categories.has(category)


# Returns the size of the Equip_Categories list
func equip_category_size() -> int:
	return Equip_Categories.size()


# Sets a new Stats_Increment dictionary
func set_stats_increment(stats_increment : Dictionary) -> void:
	Stats_Increment = stats_increment


# Sets a new increment to the given stat
func set_stat_increment(stat : String, increment : Dictionary) -> void:
	Stats_Increment[stat] = increment


# Removes an increment given the stat and returns it
func remove_stat_increment(stat : String) -> Dictionary:
	var removed_increment = Stats_Increment[stat]
	Stats_Increment.erase(stat)
	return removed_increment


# Clears the Stats_Increment dictionary and returns it
func clear_stats_increment() -> Dictionary:
	var stats_increment = Stats_Increment
	Stats_Increment.clear()
	return stats_increment


# Returns the increment given the stat
func get_stat_increment(stat : String, default = null) -> Dictionary:
	return Stats_Increment.get(stat, default)


# Returns the Stats_Increment dictionary
func get_stats_increment() -> Dictionary:
	return Stats_Increment


# Returns true if the Stats_Increment dictionary is empty
func is_stats_increment_empty() -> bool:
	return Stats_Increment.empty();


# Returns true if the Stats_Increment dictionary has the given stat
func has_stat_increment(stat : String) -> bool:
	return Stats_Increment.has(stat);


# Sets a new Skills dictionary for the class
func set_skills(skills : Dictionary) -> void:
	Skills = skills;


# Sets a new skill list for the given level
func set_skills_list(level : int, skills : Array) -> void:
	Skills[str(level)] = skills;


# Adds new skill to the given level
func add_skill(level : int, skill : String) -> void:
	if Skills.has(str(level)) == false:
		print_debug("Level not found on Skills dictionary")
	else:
		Skills[str(level)].append(skill)


# Removes the skill list for the given level and returns it
func erase_skills_list(level : int) -> Array:
	var removed_list = Skills[str(level)]
	Skills.erase(str(level))
	return removed_list


# Removes the selected skill from the given level list given its index and returns it
func remove_skill(level : int, skill_index : int) -> String:
	var removed_skill = Skills[str(level)][skill_index]
	Skills[str(level)].remove(skill_index)
	return removed_skill


# Removes the selected skill from the given level list given its name
func erase_skill(level : int, skill : String) -> void:
	Skills[str(level)].erase(skill)


# Returns the Skills dictionary
func get_skills() -> Dictionary:
	return Skills


# Returns the skills list of a given level
func get_skills_list(level : int) -> Array:
	return Skills[str(level)]


# Returns true if the Skills dictionary is empty
func is_skills_empty() -> bool:
	return Skills.empty()


# Returns true if the Skills dictionary has a list for the given level
func has_skills_list(level : int) -> bool:
	return Skills[str(level)]


# Returns true if the given list has the given skill
func has_skill(level : int, skill : String) -> bool:
	return Skills[str(level)].has(skill)


# Returns true if the given list is empty
func is_skills_list_empty(level : int) -> bool:
	return has_skills_list(level) and Skills[str(level)].empty()



#########################
#	INTERNAL METHODS	#
#########################

# Normalizes stats increment curves, making them start from level 0
func normalize_stats_increment_curves() -> void:
	for stat in Stats_Increment.keys():
		var curve = Stats_Increment[stat];
		if (curve["Start"] == 0):
			continue;
		var x1 = 1;
		var x2 = GlobalVariables.get_max_level();
		var y1 = curve["Start"];
		var y2 = curve["Finish"];
		var value_at_zero = (y2 - y1) / (x2 - x1) * -(x1) + y1;
		print_debug(Name + " " + stat + " " + str(value_at_zero));
		Stats_Increment[stat]["Start"] = value_at_zero;
