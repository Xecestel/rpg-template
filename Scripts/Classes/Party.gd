class_name Party

# Variables

var Party_Members : Array;
var Party_Name : String;
var Party_Size : int = 0;
var Formation_Size : int = 0;
var Formation : Array = [];

###########
	
	
#############################
#		PARTY HANDLING		#
#############################

# Sets the party members list
func set_party_members(members_list : Array) -> void:
	Party_Members = members_list;

# Returns the party members list
func get_party_members() -> Array:
	return Party_Members;

# Returns a party member object by passing its index on the array
func get_party_member_by_index(member_idx : int) -> PlayableCharacter:
#	var member = 
	var max_party = GlobalVariables.get_party_max_num();
#	if (party_member_idx >= 0 && party_member_idx <= max_party):
	return Party_Members[member_idx];

#Returns a party member object by passing its character id
func get_party_member(member_id : String) -> PlayableCharacter:
	var member = null;
	if (Party.has(member_id)):
		member = GlobalVariables.get_character(member_id);
		
	return member;
#	var party_member_idx = Party_Members.find(party_member);
#	if (party_member_idx >= 0):
#		return get_party_member(party_member_idx);
	
#	print_debug("Character not found in party: " + party_member_name);
#	return null;

#Returns the name of a party member
func get_party_member_name(party_member_idx : int) -> String:
	return Party_Members[party_member_idx].get_character_name();

#Given a party member PlayableCharacter object, returns the index on the party array
func get_party_member_index(party_member : PlayableCharacter) -> int:
	return Party_Members.find(party_member);

#Given a party member name, returns the index on the party array
func get_party_member_index_by_name(party_member_name : String) -> int:
	var party_member = GlobalVariables.get_character(party_member_name);
	return get_party_member_index(party_member);

#Returns true if the given PlayableCharacter object is a party member
func is_party_member(party_member : PlayableCharacter) -> bool:
	return Party_Members.has(party_member);

#Returns true if the given character passed by its name is a party member
func is_party_member_by_name(party_member_name : String) -> bool:
	var party_member = GlobalVariables.get_character(party_member_name);
	return is_party_member(party_member);

#Sets a party member by passing an index and its PlayableCharacter object
func set_party_member(party_member_idx : int, party_member : PlayableCharacter) -> void:
	if (party_member == null):
		print_debug("Invalid character passed");
		return;

	if (party_member_idx <= Party_Size):
		Party_Members[party_member_idx] = party_member;

#Sets a party member by passing an index and its name
func set_party_member_by_name(party_member_idx : int, party_member_name : String) -> void:
	var party_member = GlobalVariables.get_character(party_member_name);
	set_party_member(party_member_idx, party_member);

#Adds a character in the party by passing its PlayableCharacter object
func add_party_member(party_member : PlayableCharacter) -> void:
	if (is_party_full() || Party_Members.has(party_member)):
		return;
	Party_Members.append(party_member);
	Party_Size += 1;
	add_formation_member(party_member);

#Adds a character in the party by passing its name
func add_party_member_by_name(party_member_name : String) -> void:
	var party_member = GlobalVariables.get_character(party_member_name);
	add_party_member(party_member);

#Removes a character from the party by passing its PlayableCharacter object
func remove_party_member(party_member : PlayableCharacter) -> void:
	var party_member_index = get_party_member_index(party_member);
	if (party_member_index >= 0):
		party_member.remove(party_member_index);
		Party_Size -= 1;
		remove_formation_member(party_member);

#Removes a character from the party by passing its name
func remove_party_member_by_name(party_member_name : String) -> void:
	var party_member = GlobalVariables.get_character(party_member_name);
	remove_party_member(party_member);

#Returns the party name
func get_party_name() -> String:
	return Party_Name;

#Set a party name
func set_party_name(party_name : String) -> void:
	Party_Name = party_name;

func is_party_full() -> bool:
	var party_max_num = GlobalVariables.get_party_max_num();
	return (party_max_num > 0 && Party_Size >= party_max_num);

func get_party_size() -> int:
	return Party_Size;

#################################
#		FORMATION HANDLING		#
#################################
#Returns the formation as an array
func get_formation() -> Array:
	return Formation;

#Returns the formation member by its index
func get_formation_member(character_index : int) -> PlayableCharacter:
	return Formation[character_index];

#Returns the formation member by its name
func get_formation_member_by_name(character_name : String) -> PlayableCharacter:
	var formation_member = GlobalVariables.get_character(character_name);
	var character_index = formation.find(formation_member);
	if (character_index >= 0):
		return get_formation_member(character_index);
	print_debug("Character not found in formation: " + character_name);
	return null;

#Returns the formation member index
func get_formation_member_index(character : PlayableCharacter) -> int:
	return formation.find(character);

#Returns the formation member index by passing its name
func get_formation_member_index_by_name(character_name : String) -> int:
	var character = GlobalVariables.get_character(character_name);
	return get_formation_member_index(character);

#Given an index in the formation array, returns character name
func get_formation_member_name(formation_member_idx : int) -> String:
	return formation[formation_member_idx].get_character_name();

#Adds a given PlayableCharacter in the formation
func add_formation_member(character : PlayableCharacter) -> void:
	if (is_formation_full() || formation.has(character)):
		return;
	formation.append(character);
	formation_size += 1;
	add_party_member(character);

#Adds a given character in the formation by passing its name
func add_formation_member_by_name(character_name : String) -> void:
	var character = GlobalVariables.get_character(character_name);
	add_formation_member(character);

#Removes a formation member
func remove_formation_member(character : PlayableCharacter) -> void:
	var formation_member_index = get_formation_member_index(character);
	if (formation_member_index >= 0):
		formation.remove(formation_member_index);
		formation_size -= 1;

#Removes a formation member by passing its name 
func remove_formation_member_by_name(character_name : String) -> void:
	var formation_member = GlobalVariables.get_character(character_name);
	remove_party_member(formation_member);

#Returns true if the given character is in formation
func is_in_formation(character : PlayableCharacter) -> bool:
	return formation.has(character);

#Returns ture if the given character passed by its name is in formation
func is_in_formation_by_name(character_name : String) -> bool:
	var character = GlobalVariables.get_character(character_name);
	return is_in_formation(character);

#Sets a formation member by passing an index and its PlayableCharacter object
func set_formation_member(formation_member_idx : int, character : PlayableCharacter) -> void:
	if (character == null):
		print_debug("Invalid character passed");
		return;
	if (formation_member_idx <= formation_size):
		formation[formation_member_idx] = character;

#Sets a formation member by passing an index and its name
func set_formation_member_by_name(formation_member_idx : int, character_name : String) -> void:
	var character = GlobalVariables.get_character(character_name);
	set_formation_member(formation_member_idx, character);

func is_formation_full() -> bool:
	return (formation_size >= GlobalVariables.get_formation_max_num());

func get_formation_size() -> int:
	return formation_size;


#########################
#	SAVING AND LOADING	#
#########################

# Returns a save dictionary for saving purposes
func save():
	var save_dict = {
		"party_name"			:	get_party_name(),
		"PARTY_MEMBERS"			:	get_party_members(),
		"party_size"			:	get_party_size(),
		"formation"				:	get_formation(),
		"formation_size"		:	get_formation_size()
		}
	return save_dict;


# Loads data from save file
func load_data(save_file : File) -> bool:
	if (save_file == null):
		return false;
	
	var current_line = parse_json(save_file.get_line());
	
	for i in current_line.keys():
		if (i == "filename"):
			continue
		self.set(i, current_line[i]);
		
	return true;
