extends Node

#variables#
export (String, DIR) var maps_directory;
export (NodePath) var player; # The player node

# Node variables
onready var dialog_manager = self.get_node("UI/Dialogue");
onready var transition_mgr = self.get_node("UI/TransitionMgr");
onready var hud = self.get_node("UI/HUD");
############

#signals#
signal fade_in_finished();
signal fade_out_finished();

# Called when the node enters the scene tree for the first time.
func _ready():
#	transition_mgr.set_visible(true);
	initiate_dialog("variable_check")
	connect_signals();
	
func connect_signals() -> void:
	transition_mgr.connect("animation_finished", self, "_on_TransitionMgr_animation_finished");
	transition_mgr.connect("animation_started", self, "_on_TransitionMgr_animation_started");
	dialog_manager.connect("dialogue_started", self, "_on_Dialogue_dialogue_started");
	dialog_manager.connect("dialogue_ended", self, "_on_Dialogue_dialogue_ended");


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func change_map(map : String) -> void:
	# Attempts to open the map as a PackedResource
	# If it doesn't exist, prints error
	# If it exists, tells the resource loader to start loading
	# transition_mgr.fade_out();
	# While the resource is loading, shows the loading screen
	# Then, changes the scene
	# This means making it enter the tree, moving the player and deleting the old map
	# Hides the loading screen
	pass
	
func show_city_label(label : String) -> void:
	hud.show_city_label(label);

# Makes the dialogue manager initiate the selected dialogue
func initiate_dialog(dialog_id : String, block : String = "first") -> void:
	dialog_manager.initiate(dialog_id, block);

func _on_Dialogue_dialogue_started(dialogue_name : String):
	#player.get_input(false);
	pass

func _on_Dialogue_dialogue_ended(dialogue_name : String):
	#player.get_input(true)
	pass


func _on_TransitionMgr_animation_started(anim_name : String):
	pass # Replace with function body.


func _on_TransitionMgr_animation_finished(anim_name : String):
	match anim_name:
		"fade_in":
			emit_signal("fade_in_finished");
		"fade_out":
			emit_signal("fade_out_finished");
