extends Control

#constants#
const DEFAULT_MESSAGE_LABEL_SIZE = Vector2(1468, 229);
###########

#variables#
#Configuration variables#
export (String, FILE) var characters_showing_sound = "";
export (int) var max_lines_visible			= -1;
export (float) var message_characters_showing_cooldown = 0.2;
export (bool) var show_all_message_at_once	= true;

#Nodes variables#
onready var left_portrait						= get_node("LeftPortrait");
onready var right_portrait						= get_node("RightPortrait");
onready var message_face_rect					= get_node("MessageWindow/MessageFace");
onready var message_label						= get_node("MessageWindow/MessageText");
onready var name_tab							= get_node("MessageWindow/NameTab");
onready var name_label							= get_node("MessageWindow/NameTab/NameLabel");
onready var character_showing_cooldown_timer	= get_node("CharacterShowingCooldown");
onready var continue_button						= get_node("MessageWindow/ContinueButton");

#Internal variables#
var show_portraits			= true;
var show_face					= true;
var show_name					= true;

var default_message_text : String = "";
var default_speaker_name : String = "";
var default_message_face : Resource = null;
var default_left_portrait : PackedScene = null;
var default_right_portrait : PackedScene = null;

####################

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if (characters_showing_sound != ""):
		SoundManager.preinstantiate_node_from_string(characters_showing_sound, "sfx");
#	message_label.set_max_lines_visible(max_lines_visible);
	set_default_visibility();
	set_process(false);

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta : float) -> void:
	if (character_showing_cooldown_timer.get_time_left() == 0):
		var visible_characters = message_label.get_visible_characters() + 1;
		message_label.set_visible_characters(visible_characters);
		if (visible_characters >= message_label.get_text().replace(" ", "").length()):
			self.set_process(false);
			SoundManager.stop_se(characters_showing_sound);
			return;
		if (characters_showing_sound != "" ):
			SoundManager.play_se(characters_showing_sound);
		character_showing_cooldown_timer.start(message_characters_showing_cooldown);

#Shows given message
#If no message or text name has been passed, the script uses the default ones
func show_message(message_text : String = "", speaker_name : String = "", message_face : String = "", message_left_portrait : PackedScene = null, message_right_portrait : PackedScene = null) -> void:
	var number_of_characters_showing = 0;
	var text = message_text if (message_text != "") else default_message_text;
	var name_of_speaker = speaker_name if (speaker_name != "") else default_speaker_name;
	var face_texture = load(message_face) if (message_face != "") else default_message_face;
	var left_portrait_res = message_left_portrait if (message_left_portrait != null) else default_left_portrait;
	var right_portrait_res = message_right_portrait if (message_right_portrait != null) else default_right_portrait;
	
	if (show_all_message_at_once):
		number_of_characters_showing = text.length();
	else:
		set_process(true);
	message_label.set_visible_characters(number_of_characters_showing);
	message_label.set_bbcode(text);
	left_portrait = left_portrait_res;
	right_portrait = right_portrait_res;
	if (name_of_speaker == ""):
		set_name_visibility(false);
	else:
		set_name_visibility(true);
		name_label.set_text(name_of_speaker);
	if (face_texture == null):
		set_face_visibility(false);
	else:
		set_face_visibility(true);
		message_face_rect.set_texture(face_texture);
	set_visible(true);
	continue_button.grab_focus();
	
func close_message() -> void:
	set_visible(false);
	
func set_default_message_text(message_text : String) -> void:
	default_message_text = message_text;
	
func get_default_message_text() -> String:
	return default_message_text;
	
func set_default_speaker_name(speaker_name : String) -> void:
	default_speaker_name = speaker_name;
	
func get_default_speaker_name() -> String:
	return default_speaker_name;
	
func set_default_visibility() -> void:
	set_name_visibility(show_name);
	set_face_visibility(show_face);
	
	if (show_portraits):
		left_portrait.set_visible(true);
		right_portrait.set_visible(true);
		
	set_visible(false);
	
func set_name_visibility(is_visible : bool) -> void:
	name_tab.set_visible(is_visible);
	
func set_face_visibility(is_visible : bool) -> void:
	var text_left_margin = 16;
	if (is_visible):
		text_left_margin += message_face_rect.get_position().x + (message_face_rect.get_size().x * message_face_rect.get_scale().x);
		message_label.rect_size.x -= text_left_margin;
	else:
		message_label.rect_size.x = DEFAULT_MESSAGE_LABEL_SIZE.x;
	message_label.rect_position.x = text_left_margin;
	message_face_rect.set_visible(is_visible);

func set_portraits_visibility(is_visible : bool) -> void:
	self.show_portraits = is_visible;

func _on_ContinueButton_pressed():
	if (is_processing()):
		message_label.set_visible_characters(message_label.get_text().length());
		set_process(false);
	else:
		close_message();
