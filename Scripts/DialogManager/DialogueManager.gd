extends Node

#variables#
export (String, DIR) var Dialogues_Data_Dir = "res://Scripts/DialogManager/Data/";
export (String, FILE) var Dialogues_Data_File = "res://Scripts/DialogManager/Data/dialog_data.json";
export (bool) var Show_Portraits = false;
export (bool) var Show_Faces = true;
export (bool) var Show_Names = true;

onready var message_window = self.get_node("Message");

var Dialog_Data : Dictionary;
var get_input = true;
##########

#signals#
signal dialog_start;
signal dialog_end;

# Called when the node enters the scene tree for the first time.
func _ready():
	
	self.get_data();
	self.set_message_window();
	self.initiate_dialogue(1);
	
func _input(event : InputEvent) -> void:
	if (get_input):
		if (event.is_action_pressed("ui_accept")):
			pass

func set_message_window() -> void:
	message_window.set_name_visibility(Show_Names);
	message_window.set_face_visibility(Show_Faces);
	message_window.set_portraits_visibility(Show_Portraits);

func get_data() -> void:
	Dialog_Data = self.parse_json_file(Dialogues_Data_File);

func initiate_dialogue(dialog_id : int, stop_player : bool = true) -> void:
	var dialog_key = str(dialog_id);
	if !(Dialog_Data.keys().has(dialog_key)):
		print_debug("Dialogue not found");
		return;
		
	emit_signal("dialog_start");
	var dialog_data_path = Dialogues_Data_Dir + Dialog_Data[dialog_key];
	var dialog_data = self.parse_json_file(dialog_data_path);
	if (dialog_data.has("Error")):
		print_debug(dialog_data_path.get("Error"));
	
	for id in dialog_data.keys():
		var dialog_dic = dialog_data[id];
		message_window.show_message(dialog_dic.get("text"), dialog_dic.get("speaker_name"), dialog_dic.get("face", ""), dialog_dic.get("portrait"));
	emit_signal("dialog_end");

# Parses a json file from path
func parse_json_file(file_path : String) -> Dictionary:
	var error_dic = {"Error" : ""};
	var file = File.new();
	if (file.file_exists(file_path) == false):
		error_dic["Error"] = "Unable to open file";
		return error_dic;
	
	file.open(file_path, File.READ);
	var parse = JSON.parse(file.get_as_text());
	file.close();
	
	if (parse.get_error() != OK):
		error_dic["Error"] = parse.get_error_string();
		return error_dic;
	
	var result = parse.get_result();
	if (!(result is Dictionary)):
		error_dic["Error"] = "An error occurred when trying to access data";
		return error_dic;
		
	return result;
		
