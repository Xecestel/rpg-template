extends Node

#constants#
onready var PATH = ProjectSettings.get_setting("game_options/save_files_location");
const AUTOLOADS_TO_SAVE = [GlobalVariables, PartyManager, GameProgression];
##########

#Saves game data
func save_game() -> void:
	var save_game = File.new();
	save_game.open(PATH, File.WRITE);
	var data;
	for autoload in AUTOLOADS_TO_SAVE:
		data = autoload.save();
		save_game.store_line(to_json(data));
	save_game.close();

#Loads game data
func load_game() -> bool:
	print(OS.get_user_data_dir());
	var save_game = File.new()
	if not save_game.file_exists(PATH):
		return false #Error! Nothing to load!
	
	save_game.open(PATH, File.READ);
	for autoload in AUTOLOADS_TO_SAVE:
		if (autoload.load_data(save_game) == false):
			print_debug("Error occurred while loading " + str(autoload));
			return false; #Error loading file
	save_game.close();
	return true;
