extends ColorRect


#variables#
onready var animation = self.get_node("AnimationPlayer");
###########

#signals#
signal animation_started(anim_name);
signal animation_finished(anim_name);

func fade_in() -> void:
	animation.play("fade_in");

func fade_out() -> void:
	animation.play("fade_out");


func _on_AnimationPlayer_animation_started(anim_name : String):
	emit_signal("animation_started", anim_name);


func _on_AnimationPlayer_animation_finished(anim_name : String):
	emit_signal("animation_finished", anim_name);
