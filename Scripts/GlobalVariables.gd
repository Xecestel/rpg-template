extends Node

# Constants

const ITEMS_DATA_FILE = "res://Data/items_data.json";
const CLASSES_DATA_FILE = "res://Data/classes_data.json";
const CHARACTERS_DATA_FILE = "res://Data/characters_data.json";
const DIALOGUES_DATA_FILE = "res://Data/DialoguesData/dialog_data.json";
const SKILLS_DATA_FILE = "res://Data/skills_data.json";

##########


# Variables
onready var UNIT_SIZE = ProjectSettings.get_setting("Template Settings/General Settings/unit_size");
onready var FORMATION_MAX_NUM = ProjectSettings.get_setting("Template Settings/Party/formation_max_number");
onready var PARTY_MAX_NUM = ProjectSettings.get_setting("Template Settings/Party/party_max_number"); #Any number below 1 means no limit
onready var WEIGHT_ENABLED = ProjectSettings.get_setting("Template Settings/Items/enable_items_weight");
onready var CREATE_CHARACTERS_AT_RUNTIME = ProjectSettings.get_setting("Template Settings/General Settings/create_characters_at_runtime");
onready var SAVE_FROM_PAUSE_MENU_ENABLED = ProjectSettings.get_setting("Template Settings/General Settings/enable_save_from_pause_menu");
onready var DURABILITY_ENABLED = ProjectSettings.get_setting("Template Settings/Items/enable_items_durability");
onready var MAX_LEVEL = ProjectSettings.get_setting("Template Settings/General Settings/max_level");

var bgm_level			=	80;
var bgs_level			=	80;
var sfx_level			=	80;
var mfx_level			=	80;
var mute				=	false;
var window_mode_id		=	0;
var vsync_enabled		=	true;

var CHARACTERS_LIST : Dictionary = {};
var ITEMS_LIST : Dictionary = {};
var CLASSES_LIST : Dictionary = {};
var DIALOGUES_LIST : Dictionary = {};
var SKILLS_LIST : Dictionary = {};

###########


#################################
#		SETTINGS HANDLING		#
#################################

# Returns saved bgm level
func get_bgm_level() -> int:
	return bgm_level;


# Returns saved bgs level
func get_bgs_level() -> int:
	return bgs_level;


# Returns saved sfx level
func get_sfx_level() -> int:
	return sfx_level;


# Returns saved mfx level
func get_mfx_level() -> int:
	return mfx_level;


# Returns true if the game is saved as muted
func is_mute() -> bool:
	return mute;


# Returns saved window mode id
func get_window_mode_id() -> int:
	return window_mode_id;


# Returns true if vsync is saved as enabled
func is_vsync_enabled() -> bool:
	return vsync_enabled;



#################################
#		CONSTANTS GETTERS		#
#################################

# Returns the max level reachable
func get_max_level() -> int:
	return MAX_LEVEL;


# Returs game unit size
func get_unit_size() -> int:
	return UNIT_SIZE;


# Returns the max number available for battle formation
func get_formation_max_num() -> int:
	return FORMATION_MAX_NUM;


# Returns the max number available for party members
func get_party_max_num() -> int:
	return PARTY_MAX_NUM;


# Returns the path to the items data file
func get_items_data_path() -> String:
	return self.ITEMS_DATA_PATH;


# Returns the path to the characters data file
func get_characters_data_path() -> String:
	return self.CHARACTERS_DATA_PATH;


# Returns the path to the classes data file
func get_classes_data_path() -> String:
	return self.CLASSES_DATA_PATH;


# Returns true if weight is enabled for items
func is_weight_enabled() -> bool:
	return WEIGHT_ENABLED;


# Returns true if characters are being created at runtime
func is_create_characters_at_runtime() -> bool:
	return CREATE_CHARACTERS_AT_RUNTIME;


# Returns true if save is enabled from pause menu
func is_save_from_pause_menu_enabled() -> bool:
	return SAVE_FROM_PAUSE_MENU_ENABLED;


# Returns true if the items durability has been enabled
func is_items_durability_enabled() -> bool:
	return DURABILITY_ENABLED;


#########################################
#		 CHARACTERS LIST HANDLING		#
#########################################

# Given the id of a playable character, returns the object from the list
func get_character(character_id : String):
	return CHARACTERS_LIST.get(character_id);


# Returns the playable characters list as a string
func get_characters_list_as_string() -> String:
	return str(CHARACTERS_LIST);


# Adds a character on the characters list
func add_character(character, character_id : String = "") -> void:
	if (character_id == ""):
		character_id = character.get_name();
	if (CHARACTERS_LIST.has(character_id) == false):
		CHARACTERS_LIST[character_id] = character;


# Remove a character from the characters list by passing the object
func remove_character(character) -> bool:
	return remove_character_by_id(character.get_name());


# Remove a character from the characters list by passing its id
# Returns true if the character has been replaced
func remove_character_by_id(character_id : String) -> bool:
	return CHARACTERS_LIST.erase(character_id);


# Returns the characters list
func get_characters_list() -> Dictionary:
	return CHARACTERS_LIST;


# Returns an array containing the id of all the characters within the game
func get_characters_ids() -> Array:
	return CHARACTERS_LIST.keys();


# Converts all the dictionaries in the characters list in an object
func convert_characters_list() -> void:
	for id in CHARACTERS_LIST.keys():
		var character_data = CHARACTERS_LIST[id];
		if (!(character_data is Dictionary)):
			print_debug("Character with id " + id + " is not a dictionary.")
			print_debug("Continuing.")
			continue;
			
		var character = null;
		
		var type : String = character_data["Type"].to_lower().replace("character", "").replace(" ", "").replace("_", "");
		match type:
			"playable":
				character = PlayableCharacter.new(character_data);
			"enemy":
				character = EnemyCharacter.new(character_data);
			"battle":
				character = BattleCharacter.new(character_data);
		
		if (character != null):
			add_character(character, id);
		else:
			print_debug("Unable to create character with id: " + id);


#################################
#		ITEMS LIST HANDLING		#
#################################

# Given the name of an item, returns the Item object from the list
func get_item(item_id : String, default = null) -> Item:
	return ITEMS_LIST.get(item_id);


# Returns the items list as a string
func get_items_list_as_string() -> String:
	return str(ITEMS_LIST);


# Adds an item on the items list
func add_item(item, item_id : String = "") -> void:
	if (item_id == ""):
		item_id = item.get_item_name();
	if (ITEMS_LIST.has(item_id) == false):
		ITEMS_LIST[item_id] = item;


# Remove an item from the list by passing the Item object
func remove_item(item : Item) -> bool:
	return remove_item_by_id(item.get_item_name());


# Remove an item from the list by passing its id
# Returns true if the Item has been deleted successfully
func remove_item_by_id(item_id : String) -> bool:
	return ITEMS_LIST.erase(item_id);


# Returns the items list
func get_items_list() -> Dictionary:
	return ITEMS_LIST;


# Returns an array containing the ids of all the items within the game
func get_items_names() -> Array:
	return ITEMS_LIST.keys();


# Converts every dictionary in the items list in an object
func convert_items_list() -> void:
	for id in ITEMS_LIST.keys():
		var item_data = ITEMS_LIST[id];
		if (!(item_data is Dictionary)):
			print_debug("Item with id " + id + " is not a dictionary.")
			print_debug("Continuing.")
			continue;
			
		var item = null;
		
		var type : String = item_data["Type"].to_lower().replace("item", "").replace(" ", "").replace("_", "");
		match type:
			"consumable":
				item = ItemConsumable.new(item_data);
			"key":
				item = ItemKey.new(item_data);
			"equippable":
				item = ItemEquippable.new(item_data);
		
		if (item != null):
			add_item(item, id);
		else:
			print_debug("Unable to create item with id: " + id);



#########################################
#		 CLASSES LIST HANDLING			#
#########################################

# Given the class name, returns the class information
func get_character_class(character_class : String) -> Class:
	return CLASSES_LIST.get(character_class);


# Returns the classes list as a string
func get_classes_list_as_string() -> String:
	return str(CLASSES_LIST);


# Adds a class on the classes list
func add_class(new_class : Class, class_id : String = "") -> void:
	if (class_id == ""):
		class_id = new_class.get_name();
	if (CHARACTERS_LIST.has(class_id) == false):
		CHARACTERS_LIST[class_id] = new_class;


# Remove a class from the classes list by passing the Class object
func remove_class(class_to_remove : Class) -> bool:
	return remove_class_by_id(class_to_remove.get_name());


# Remove a class from the classes list by passing its id
# Returns true if the Class has been removed successfully
func remove_class_by_id(class_id : String) -> bool:
	return CHARACTERS_LIST.erase(class_id);


# Returns the classes list
func get_classes_list() -> Dictionary:
	return CLASSES_LIST;


# Returns an array containing the ids of all the classes within the game
func get_classes_ids() -> Array:
	return CLASSES_LIST.keys();


# Converts all the dictionaries in the classes list in an object
func convert_classes_list() -> void:
	for id in CLASSES_LIST.keys():
		var class_data = CLASSES_LIST[id];
		if (!(class_data is Dictionary)):
			print_debug("Class with id " + id + " is not a dictionary.")
			print_debug("Continuing.")
			continue;
			
		var new_class = Class.new(class_data);
		
		if (new_class != null):
			add_class(new_class, id);
		else:
			print_debug("Unable to create class with id: " + id);



#################################
#		SKILLS LIST HANDLING	#
#################################

# Given the skill name, returns the skill information
func get_skill(skill : String) -> Skill:
	return SKILLS_LIST.get(skill);


# Returns the skills list as a string
func get_skills_list_as_string() -> String:
	return str(SKILLS_LIST);


# Adds a skill on the skills list
func add_skill(skill : Skill, skill_id : String = "") -> void:
	if (skill_id == ""):
		skill_id = skill.get_name();
	if (SKILLS_LIST.has(skill_id) == false):
		SKILLS_LIST[skill_id] = skill;


# Remove a skill from the skills list by passing the Skill object
func remove_skill(skill_to_remove : Skill) -> bool:
	return remove_skill_by_id(skill_to_remove.get_name());


# Remove a skill from the skills list by passing its id
# Returns true if the Skill has been removed successfully
func remove_skill_by_id(skill_id : String) -> bool:
	return SKILLS_LIST.erase(skill_id);


# Returns the skills list
func get_skills_list() -> Dictionary:
	return SKILLS_LIST;


# Returns an array containing the ids of all the skills within the game
func get_skills_ids() -> Array:
	return SKILLS_LIST.keys();


# Converts all the dictionaries in the skills list in an object
func convert_skills_list() -> void:
	for id in SKILLS_LIST.keys():
		var skill_data = SKILLS_LIST[id];
		if (!(skill_data is Dictionary)):
			print_debug("Skill with id " + id + " is not a dictionary.")
			print_debug("Continuing.")
			continue;
			
		var skill = Skill.new(skill_data);
		
		if (skill != null):
			add_skill(skill, id);
		else:
			print_debug("Unable to create skill with id: " + id);



#################################
#	DIALOGUES LIST HANDLING		#
#################################

# Returns a dialogue given its id
func get_dialogue(dialog_id : String) -> Dictionary:
	return DIALOGUES_LIST.get(dialog_id);


# Returns the dialogues list as a string
func get_dialogues_list_as_string() -> String:
	return str(DIALOGUES_LIST);


# Returns the dialogues list
func get_dialogues_list() -> Dictionary:
	return DIALOGUES_LIST;


# Returns an array containing the names of all the dialogues within the game
func get_dialogues_names() -> Array:
	return DIALOGUES_LIST.keys();


#########################
#	SAVING AND LOADING	#
#########################

# Loads on engine the data stored on GlobalVariables
func load_on_engine() -> void:
	TranslationServer.set_locale(self.LANGUAGES[self.language_id]);
	
	AudioServer.set_bus_mute(0, mute);

	var bgm_db = linear2db(bgm_level);
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("BGM"), bgm_db);
	
	var bgs_db = linear2db(bgs_level);
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("BGS"), bgs_db);
	
	var mfx_db = linear2db(mfx_level);
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("MFX"), mfx_db);
	
	var sfx_db = linear2db(sfx_level);
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("SFX"), sfx_db);
		
	# Changes window mode
	if (self.window_mode_id == 0): #if fullscreen selected
		OS.window_borderless = false;
		OS.window_fullscreen = false;
		
	if (self.window_mode_id == 1): #if borderless selected
		OS.window_fullscreen = false;
		OS.window_borderless = true;

	if (self.window_mode_id == 2): #if window mode selected
		OS.window_borderless = false;
		OS.window_fullscreen = true;
	
	OS.vsync_enabled = self.vsync_enabled; #enables/disables vsync


# Returns a save dictionary for saving purposes	
func save():
	var save_dict = {
		"filename"					:	get_filename(),
		"bgm_level"					:	get_bgm_level(),
		"bgs_level"					:	get_bgs_level(),
		"sfx_level"					:	get_sfx_level(),
		"mfx_level"					:	get_mfx_level(),
		"mute"						:	is_mute(),
		"window_mode_id"			:	get_window_mode_id(),
		"vsync_enabled"				:	is_vsync_enabled(),
		"PLAYABLE_CHARACTERS_LIST"	:	get_characters_list()
		}
	return save_dict;


# Loads data from save file
func load_data(save_file : File) -> bool:
	if (save_file == null):
		return false;
	
	var current_line = parse_json(save_file.get_line());
	
	for i in current_line.keys():
		if (i == "filename"):
			continue
		self.set(i, current_line[i]);
		
	return true;



#################################
#		INTERNAL METHODS		#
#################################

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	fill_list("ITEMS_LIST", ITEMS_DATA_FILE);
	convert_items_list();
	fill_list("CLASSES_LIST", CLASSES_DATA_FILE);
	convert_classes_list();
	if (is_create_characters_at_runtime() == false):
		fill_list("CHARACTERS_LIST", CHARACTERS_DATA_FILE);
		convert_characters_list();
	fill_list("DIALOGUES_LIST", DIALOGUES_DATA_FILE);
	fill_list("SKILLS_LIST", SKILLS_DATA_FILE);
	convert_skills_list();


# Fills a given string with data got from a given file
func fill_list(list : String, data_file : String) -> void:
	var dictionary = parse_json_file(data_file);
	if (dictionary.has("Error")):
		print_debug(dictionary["Error"]);
		return;
	self.set(list, dictionary)


# Parses a json file from path
func parse_json_file(file_path : String) -> Dictionary:
	var error_dic = {"Error" : ""};
	var file = File.new();
	if (file.file_exists(file_path) == false):
		error_dic["Error"] = "Unable to open file";
		return error_dic;
	
	file.open(file_path, File.READ);
	var parse = JSON.parse(file.get_as_text());
	file.close();
	
	if (parse.get_error() != OK):
		error_dic["Error"] = parse.get_error_string();
		return error_dic;
	
	var result = parse.get_result();
	if (!(result is Dictionary)):
		error_dic["Error"] = "An error occurred when trying to access data";
		return error_dic;
		
	return result;
