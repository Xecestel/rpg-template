extends Control


#variables#
export (float) var city_label_wait_time = 1.0;

onready var city_label = self.get_node("CityLabel/Label");
onready var animation = self.get_node("AnimationPlayer");
onready var city_label_wait_timer = self.get_node("CityLabel/CityTimer");

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func show_city_label(label : String) -> void:
	city_label.set_text(label);
	animation.play("pop-up");

func _on_AnimationPlayer_animation_finished(anim_name : String):
	match anim_name:
		"pop-up":
			city_label_wait_timer.start(city_label_wait_time);
		"pop-down":
			city_label.set_text("");

func _on_CityTimer_timeout():
	animation.play("pop-down");
