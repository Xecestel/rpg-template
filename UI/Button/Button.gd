extends Button

#variables#
export(String, FILE, "*.tscn") var scene_to_load;
###########

func _on_Button_pressed():
	if (scene_to_load != null && scene_to_load != ""):
		self.get_tree().change_scene(scene_to_load);
