# RPG Template
## Version 0.15
## © Xecestel
## MIT License

This is a Godot Engine template to easily develop 2D RPGs. Its purpose is to make a base, a skeleton, for future projects, being those turn-based RPGs, action RPGs or even simple story-driven games.  
Basically, if you miss RPG Maker, this is the template for you!  

If you need some help on the use of this template, check out the [official wiki docs](https://gitlab.com/Xecestel/rpg-template/wikis/docs/home)!

## Credits
Designed and programmed by Celeste Privitera (@xecestel).

This template wouldn't have been possible without the *Godot Open Dialog*, made by J.Sena and released under CC-BY 4.0 license terms.

## Licenses
RPG Template  
Copyright © 2019-2020 Celeste Privitera  
This software is an open source project licensed under MIT License. Check the LICENSE file for more info.  

This template uses the [Sound Manager Plugin](https://gitlab.com/Xecestel/sound-manager) which is licensed under MPL 2.0. Read the LICENSE file inside the plugin repository for more information.  

This template uses the [Quest Log Manager Plugin](https://gitlab.com/Xecestel/quest-log-manager) which is licensed under MIT License, Read the LICENSE file inside the plugin repository for more information.

This template uses an adapted version of the [Godot Open Dialog](https://bitbucket.org/jsena42/godot-open-dialogue/src/master/) made by J.Sena, which is licensed under CC-BY 4.0. Read the LICENSE file inside the module repository (`Script/Godot Open Dialog`) for more information.

Third parties assets are licensed under licenses chose by their author.  
This include:
- "Assets/Fonts/VarelaRound-Regular" font, made by Joe Prince and Avraham Cornfeld, licensed under OFL (Open Font License).

## Changelog

### Version 0.1
- Initial commit
- Added .gitignore
- Added CharacterBase, BattleCharacter and PlayableCharacter scripts
- Added PartyManager script
- Added GlobalVariables script

### Version 0.2
- Improved PartyManager, GlobalVariables, PlayableCharacter, CharacterBase

### Version 0.3
- Added MessagesManager, Sound Manager plugin

### Version 0.4
- Updated Sound Manager Plugin with beta version 1.10

### Version 0.5
- Added Item.gd and ItemConsumable.gd
- Added Inventory on PlayableCharacter.gd

### Version 0.5.1
- Improved Items and Players
- Added characters data and items data folders

### Version 0.6
- Added Quest and Objective
- Improved GlobalVariables script
- Added QuestTemplate and ObjectiveTemplate
- Started working on QuestLog

### Version 0.6.1
- Improved QuestLog
- Bug fixes and general improvements

### Version 0.7
- Added LogManager class: now QuestLog is a LogManager subclass
- Added Entry class: now Quest is an Entry subclass
- Minor improvements and fixes

### Version 0.7.1
- Minor improvements and fixes

### Version 0.7.2
- Exported Quest Log Class and Log Base Class as stand-alone classes

### Version 0.7.3
- [Quest Log](https://gitlab.com/Xecestel/quest-log) and [Log Base](https://gitlab.com/Xecestel/log-base) are now submodules

### Version 0.8
- Added [Quest Log Manager Plugin](https://gitlab.com/Xecestel/quest-log-manager)

### Version 0.8.1
- Bug fix

### Version 0.9
- Added Godot Open Dialog
- Added GameProgression Autoload
- Updated SaveLoad Autoload
- Added GameManager
- Added TransitionMgr
- Started working on HUD

### Version 0.10
- Updated `ProjectSettings`
- Updated autoloads accordingly
- Updated [official docs](https://gitlab.com/Xecestel/rpg-template/wikis/docs/home)

### Version 0.11
- Deleted lists filling methods on GlobalVariables: now there's only one fill_list method to avoid redundancy
- Updated data files and fill_list method: now the user doesn't use a directory for each list, but just a file for each list. This has been made to avoid too many OS calls during files opening
- Fixed some bugs in the Dialogue Manager
- Adapted the Dialogue Manager to use a single file instead of a directory
- To do: updated quest log and relative plugin

### Version 0.12
- Updated QuestLog, LogBase, Quests, Objectives and Entry classes: general improvements, improved readability, bug fixes, and switched from directory access to single file access like the other lists (see Version 0.11 changelog)
- To do: update the Quest Manager Plugin

### Version 0.13
- Updated Quest Helper Plugin
- Improved Character and Item classes
- Added EnemyCharacter class and ItemKey class
- Improved code readability

### Version 0.13.1
- Added Class script

### Version 0.14
- Added variables to dialogue system
- Reworked lists in GlobalVariables: now they actually work as intended; also added Classes list
- Added stats increment method in Classes
- Fixed some minor bugs
- Improved code readability

### Version 0.15
- Added Skills
- Added Skills list in GlobalVariables
